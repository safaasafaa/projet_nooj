/**
 */
package modelpim_2.impl;

import modelpim_2.CompositeState;
import modelpim_2.Edge;
import modelpim_2.Modelpim_2Package;
import modelpim_2.SimpleState;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelpim_2.impl.EdgeImpl#getEtatCibleSimple <em>Etat Cible Simple</em>}</li>
 *   <li>{@link modelpim_2.impl.EdgeImpl#getEtatCibleComposite <em>Etat Cible Composite</em>}</li>
 *   <li>{@link modelpim_2.impl.EdgeImpl#getEvent <em>Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EdgeImpl extends MinimalEObjectImpl.Container implements Edge {
	/**
	 * The cached value of the '{@link #getEtatCibleSimple() <em>Etat Cible Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEtatCibleSimple()
	 * @generated
	 * @ordered
	 */
	protected SimpleState etatCibleSimple;

	/**
	 * The cached value of the '{@link #getEtatCibleComposite() <em>Etat Cible Composite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEtatCibleComposite()
	 * @generated
	 * @ordered
	 */
	protected CompositeState etatCibleComposite;

	/**
	 * The default value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected String event = EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EdgeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Modelpim_2Package.Literals.EDGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleState getEtatCibleSimple() {
		if (etatCibleSimple != null && etatCibleSimple.eIsProxy()) {
			InternalEObject oldEtatCibleSimple = (InternalEObject)etatCibleSimple;
			etatCibleSimple = (SimpleState)eResolveProxy(oldEtatCibleSimple);
			if (etatCibleSimple != oldEtatCibleSimple) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Modelpim_2Package.EDGE__ETAT_CIBLE_SIMPLE, oldEtatCibleSimple, etatCibleSimple));
			}
		}
		return etatCibleSimple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleState basicGetEtatCibleSimple() {
		return etatCibleSimple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEtatCibleSimple(SimpleState newEtatCibleSimple) {
		SimpleState oldEtatCibleSimple = etatCibleSimple;
		etatCibleSimple = newEtatCibleSimple;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.EDGE__ETAT_CIBLE_SIMPLE, oldEtatCibleSimple, etatCibleSimple));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeState getEtatCibleComposite() {
		if (etatCibleComposite != null && etatCibleComposite.eIsProxy()) {
			InternalEObject oldEtatCibleComposite = (InternalEObject)etatCibleComposite;
			etatCibleComposite = (CompositeState)eResolveProxy(oldEtatCibleComposite);
			if (etatCibleComposite != oldEtatCibleComposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Modelpim_2Package.EDGE__ETAT_CIBLE_COMPOSITE, oldEtatCibleComposite, etatCibleComposite));
			}
		}
		return etatCibleComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeState basicGetEtatCibleComposite() {
		return etatCibleComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEtatCibleComposite(CompositeState newEtatCibleComposite) {
		CompositeState oldEtatCibleComposite = etatCibleComposite;
		etatCibleComposite = newEtatCibleComposite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.EDGE__ETAT_CIBLE_COMPOSITE, oldEtatCibleComposite, etatCibleComposite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(String newEvent) {
		String oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.EDGE__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Modelpim_2Package.EDGE__ETAT_CIBLE_SIMPLE:
				if (resolve) return getEtatCibleSimple();
				return basicGetEtatCibleSimple();
			case Modelpim_2Package.EDGE__ETAT_CIBLE_COMPOSITE:
				if (resolve) return getEtatCibleComposite();
				return basicGetEtatCibleComposite();
			case Modelpim_2Package.EDGE__EVENT:
				return getEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Modelpim_2Package.EDGE__ETAT_CIBLE_SIMPLE:
				setEtatCibleSimple((SimpleState)newValue);
				return;
			case Modelpim_2Package.EDGE__ETAT_CIBLE_COMPOSITE:
				setEtatCibleComposite((CompositeState)newValue);
				return;
			case Modelpim_2Package.EDGE__EVENT:
				setEvent((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Modelpim_2Package.EDGE__ETAT_CIBLE_SIMPLE:
				setEtatCibleSimple((SimpleState)null);
				return;
			case Modelpim_2Package.EDGE__ETAT_CIBLE_COMPOSITE:
				setEtatCibleComposite((CompositeState)null);
				return;
			case Modelpim_2Package.EDGE__EVENT:
				setEvent(EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Modelpim_2Package.EDGE__ETAT_CIBLE_SIMPLE:
				return etatCibleSimple != null;
			case Modelpim_2Package.EDGE__ETAT_CIBLE_COMPOSITE:
				return etatCibleComposite != null;
			case Modelpim_2Package.EDGE__EVENT:
				return EVENT_EDEFAULT == null ? event != null : !EVENT_EDEFAULT.equals(event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (event: ");
		result.append(event);
		result.append(')');
		return result.toString();
	}

} //EdgeImpl
