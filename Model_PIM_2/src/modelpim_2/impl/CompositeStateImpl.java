/**
 */
package modelpim_2.impl;

import java.math.BigInteger;
import java.util.Collection;

import modelpim_2.CompositeState;
import modelpim_2.Diagram;
import modelpim_2.Edge;
import modelpim_2.Modelpim_2Package;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelpim_2.impl.CompositeStateImpl#getCmpDiags <em>Cmp Diags</em>}</li>
 *   <li>{@link modelpim_2.impl.CompositeStateImpl#getTransOut2 <em>Trans Out2</em>}</li>
 *   <li>{@link modelpim_2.impl.CompositeStateImpl#getName <em>Name</em>}</li>
 *   <li>{@link modelpim_2.impl.CompositeStateImpl#getAbs <em>Abs</em>}</li>
 *   <li>{@link modelpim_2.impl.CompositeStateImpl#getOrd <em>Ord</em>}</li>
 *   <li>{@link modelpim_2.impl.CompositeStateImpl#getSymbole <em>Symbole</em>}</li>
 *   <li>{@link modelpim_2.impl.CompositeStateImpl#getNbEtatLie <em>Nb Etat Lie</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CompositeStateImpl extends MinimalEObjectImpl.Container implements CompositeState {
	/**
	 * The cached value of the '{@link #getCmpDiags() <em>Cmp Diags</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCmpDiags()
	 * @generated
	 * @ordered
	 */
	protected EList cmpDiags;

	/**
	 * The cached value of the '{@link #getTransOut2() <em>Trans Out2</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransOut2()
	 * @generated
	 * @ordered
	 */
	protected EList transOut2;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getAbs() <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbs()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ABS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAbs() <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbs()
	 * @generated
	 * @ordered
	 */
	protected BigInteger abs = ABS_EDEFAULT;

	/**
	 * The default value of the '{@link #getOrd() <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrd()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ORD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOrd() <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrd()
	 * @generated
	 * @ordered
	 */
	protected BigInteger ord = ORD_EDEFAULT;

	/**
	 * The default value of the '{@link #getSymbole() <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbole()
	 * @generated
	 * @ordered
	 */
	protected static final int SYMBOLE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSymbole() <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbole()
	 * @generated
	 * @ordered
	 */
	protected int symbole = SYMBOLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNbEtatLie() <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbEtatLie()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_ETAT_LIE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNbEtatLie() <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbEtatLie()
	 * @generated
	 * @ordered
	 */
	protected int nbEtatLie = NB_ETAT_LIE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Modelpim_2Package.Literals.COMPOSITE_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getCmpDiags() {
		if (cmpDiags == null) {
			cmpDiags = new EObjectContainmentEList(Diagram.class, this, Modelpim_2Package.COMPOSITE_STATE__CMP_DIAGS);
		}
		return cmpDiags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getTransOut2() {
		if (transOut2 == null) {
			transOut2 = new EObjectContainmentEList(Edge.class, this, Modelpim_2Package.COMPOSITE_STATE__TRANS_OUT2);
		}
		return transOut2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.COMPOSITE_STATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getAbs() {
		return abs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbs(BigInteger newAbs) {
		BigInteger oldAbs = abs;
		abs = newAbs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.COMPOSITE_STATE__ABS, oldAbs, abs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getOrd() {
		return ord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrd(BigInteger newOrd) {
		BigInteger oldOrd = ord;
		ord = newOrd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.COMPOSITE_STATE__ORD, oldOrd, ord));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSymbole() {
		return symbole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbole(int newSymbole) {
		int oldSymbole = symbole;
		symbole = newSymbole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.COMPOSITE_STATE__SYMBOLE, oldSymbole, symbole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbEtatLie() {
		return nbEtatLie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNbEtatLie(int newNbEtatLie) {
		int oldNbEtatLie = nbEtatLie;
		nbEtatLie = newNbEtatLie;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.COMPOSITE_STATE__NB_ETAT_LIE, oldNbEtatLie, nbEtatLie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Modelpim_2Package.COMPOSITE_STATE__CMP_DIAGS:
				return ((InternalEList)getCmpDiags()).basicRemove(otherEnd, msgs);
			case Modelpim_2Package.COMPOSITE_STATE__TRANS_OUT2:
				return ((InternalEList)getTransOut2()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Modelpim_2Package.COMPOSITE_STATE__CMP_DIAGS:
				return getCmpDiags();
			case Modelpim_2Package.COMPOSITE_STATE__TRANS_OUT2:
				return getTransOut2();
			case Modelpim_2Package.COMPOSITE_STATE__NAME:
				return getName();
			case Modelpim_2Package.COMPOSITE_STATE__ABS:
				return getAbs();
			case Modelpim_2Package.COMPOSITE_STATE__ORD:
				return getOrd();
			case Modelpim_2Package.COMPOSITE_STATE__SYMBOLE:
				return new Integer(getSymbole());
			case Modelpim_2Package.COMPOSITE_STATE__NB_ETAT_LIE:
				return new Integer(getNbEtatLie());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Modelpim_2Package.COMPOSITE_STATE__CMP_DIAGS:
				getCmpDiags().clear();
				getCmpDiags().addAll((Collection)newValue);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__TRANS_OUT2:
				getTransOut2().clear();
				getTransOut2().addAll((Collection)newValue);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__NAME:
				setName((String)newValue);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__ABS:
				setAbs((BigInteger)newValue);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__ORD:
				setOrd((BigInteger)newValue);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__SYMBOLE:
				setSymbole(((Integer)newValue).intValue());
				return;
			case Modelpim_2Package.COMPOSITE_STATE__NB_ETAT_LIE:
				setNbEtatLie(((Integer)newValue).intValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Modelpim_2Package.COMPOSITE_STATE__CMP_DIAGS:
				getCmpDiags().clear();
				return;
			case Modelpim_2Package.COMPOSITE_STATE__TRANS_OUT2:
				getTransOut2().clear();
				return;
			case Modelpim_2Package.COMPOSITE_STATE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__ABS:
				setAbs(ABS_EDEFAULT);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__ORD:
				setOrd(ORD_EDEFAULT);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__SYMBOLE:
				setSymbole(SYMBOLE_EDEFAULT);
				return;
			case Modelpim_2Package.COMPOSITE_STATE__NB_ETAT_LIE:
				setNbEtatLie(NB_ETAT_LIE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Modelpim_2Package.COMPOSITE_STATE__CMP_DIAGS:
				return cmpDiags != null && !cmpDiags.isEmpty();
			case Modelpim_2Package.COMPOSITE_STATE__TRANS_OUT2:
				return transOut2 != null && !transOut2.isEmpty();
			case Modelpim_2Package.COMPOSITE_STATE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Modelpim_2Package.COMPOSITE_STATE__ABS:
				return ABS_EDEFAULT == null ? abs != null : !ABS_EDEFAULT.equals(abs);
			case Modelpim_2Package.COMPOSITE_STATE__ORD:
				return ORD_EDEFAULT == null ? ord != null : !ORD_EDEFAULT.equals(ord);
			case Modelpim_2Package.COMPOSITE_STATE__SYMBOLE:
				return symbole != SYMBOLE_EDEFAULT;
			case Modelpim_2Package.COMPOSITE_STATE__NB_ETAT_LIE:
				return nbEtatLie != NB_ETAT_LIE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", abs: ");
		result.append(abs);
		result.append(", ord: ");
		result.append(ord);
		result.append(", symbole: ");
		result.append(symbole);
		result.append(", nbEtatLie: ");
		result.append(nbEtatLie);
		result.append(')');
		return result.toString();
	}

} //CompositeStateImpl
