/**
 */
package modelpim_2.impl;

import java.util.Collection;
import modelpim_2.CompositeState;
import modelpim_2.Diagram;
import modelpim_2.Modelpim_2Package;
import modelpim_2.Note;
import modelpim_2.SimpleState;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Diagram</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelpim_2.impl.DiagramImpl#getSimpleStates <em>Simple States</em>}</li>
 *   <li>{@link modelpim_2.impl.DiagramImpl#getCompositeStates <em>Composite States</em>}</li>
 *   <li>{@link modelpim_2.impl.DiagramImpl#getName <em>Name</em>}</li>
 *   <li>{@link modelpim_2.impl.DiagramImpl#getNotes <em>Notes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DiagramImpl extends MinimalEObjectImpl.Container implements Diagram {
	/**
	 * The cached value of the '{@link #getSimpleStates() <em>Simple States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleStates()
	 * @generated
	 * @ordered
	 */
	protected EList simpleStates;

	/**
	 * The cached value of the '{@link #getCompositeStates() <em>Composite States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompositeStates()
	 * @generated
	 * @ordered
	 */
	protected EList compositeStates;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNotes() <em>Notes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotes()
	 * @generated
	 * @ordered
	 */
	protected EList notes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiagramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Modelpim_2Package.Literals.DIAGRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getSimpleStates() {
		if (simpleStates == null) {
			simpleStates = new EObjectContainmentEList(SimpleState.class, this, Modelpim_2Package.DIAGRAM__SIMPLE_STATES);
		}
		return simpleStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getCompositeStates() {
		if (compositeStates == null) {
			compositeStates = new EObjectContainmentEList(CompositeState.class, this, Modelpim_2Package.DIAGRAM__COMPOSITE_STATES);
		}
		return compositeStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpim_2Package.DIAGRAM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getNotes() {
		if (notes == null) {
			notes = new EObjectContainmentEList(Note.class, this, Modelpim_2Package.DIAGRAM__NOTES);
		}
		return notes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Modelpim_2Package.DIAGRAM__SIMPLE_STATES:
				return ((InternalEList)getSimpleStates()).basicRemove(otherEnd, msgs);
			case Modelpim_2Package.DIAGRAM__COMPOSITE_STATES:
				return ((InternalEList)getCompositeStates()).basicRemove(otherEnd, msgs);
			case Modelpim_2Package.DIAGRAM__NOTES:
				return ((InternalEList)getNotes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Modelpim_2Package.DIAGRAM__SIMPLE_STATES:
				return getSimpleStates();
			case Modelpim_2Package.DIAGRAM__COMPOSITE_STATES:
				return getCompositeStates();
			case Modelpim_2Package.DIAGRAM__NAME:
				return getName();
			case Modelpim_2Package.DIAGRAM__NOTES:
				return getNotes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Modelpim_2Package.DIAGRAM__SIMPLE_STATES:
				getSimpleStates().clear();
				getSimpleStates().addAll((Collection)newValue);
				return;
			case Modelpim_2Package.DIAGRAM__COMPOSITE_STATES:
				getCompositeStates().clear();
				getCompositeStates().addAll((Collection)newValue);
				return;
			case Modelpim_2Package.DIAGRAM__NAME:
				setName((String)newValue);
				return;
			case Modelpim_2Package.DIAGRAM__NOTES:
				getNotes().clear();
				getNotes().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Modelpim_2Package.DIAGRAM__SIMPLE_STATES:
				getSimpleStates().clear();
				return;
			case Modelpim_2Package.DIAGRAM__COMPOSITE_STATES:
				getCompositeStates().clear();
				return;
			case Modelpim_2Package.DIAGRAM__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Modelpim_2Package.DIAGRAM__NOTES:
				getNotes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Modelpim_2Package.DIAGRAM__SIMPLE_STATES:
				return simpleStates != null && !simpleStates.isEmpty();
			case Modelpim_2Package.DIAGRAM__COMPOSITE_STATES:
				return compositeStates != null && !compositeStates.isEmpty();
			case Modelpim_2Package.DIAGRAM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Modelpim_2Package.DIAGRAM__NOTES:
				return notes != null && !notes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DiagramImpl
