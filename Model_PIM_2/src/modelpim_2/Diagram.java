/**
 */
package modelpim_2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpim_2.Diagram#getSimpleStates <em>Simple States</em>}</li>
 *   <li>{@link modelpim_2.Diagram#getCompositeStates <em>Composite States</em>}</li>
 *   <li>{@link modelpim_2.Diagram#getName <em>Name</em>}</li>
 *   <li>{@link modelpim_2.Diagram#getNotes <em>Notes</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpim_2.Modelpim_2Package#getDiagram()
 * @model
 * @generated
 */
public interface Diagram extends EObject {
	/**
	 * Returns the value of the '<em><b>Simple States</b></em>' containment reference list.
	 * The list contents are of type {@link modelpim_2.SimpleState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple States</em>' containment reference list.
	 * @see modelpim_2.Modelpim_2Package#getDiagram_SimpleStates()
	 * @model type="modelpim_2.SimpleState" containment="true" required="true"
	 * @generated
	 */
	EList getSimpleStates();

	/**
	 * Returns the value of the '<em><b>Composite States</b></em>' containment reference list.
	 * The list contents are of type {@link modelpim_2.CompositeState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite States</em>' containment reference list.
	 * @see modelpim_2.Modelpim_2Package#getDiagram_CompositeStates()
	 * @model type="modelpim_2.CompositeState" containment="true" required="true"
	 * @generated
	 */
	EList getCompositeStates();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelpim_2.Modelpim_2Package#getDiagram_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelpim_2.Diagram#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Notes</b></em>' containment reference list.
	 * The list contents are of type {@link modelpim_2.Note}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Notes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Notes</em>' containment reference list.
	 * @see modelpim_2.Modelpim_2Package#getDiagram_Notes()
	 * @model type="modelpim_2.Note" containment="true"
	 * @generated
	 */
	EList getNotes();

} // Diagram
