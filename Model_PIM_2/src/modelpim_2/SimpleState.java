/**
 */
package modelpim_2;

import java.math.BigInteger;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpim_2.SimpleState#getTransOut1 <em>Trans Out1</em>}</li>
 *   <li>{@link modelpim_2.SimpleState#getName <em>Name</em>}</li>
 *   <li>{@link modelpim_2.SimpleState#getAbs <em>Abs</em>}</li>
 *   <li>{@link modelpim_2.SimpleState#getOrd <em>Ord</em>}</li>
 *   <li>{@link modelpim_2.SimpleState#getSymbole <em>Symbole</em>}</li>
 *   <li>{@link modelpim_2.SimpleState#getNbEtatLie <em>Nb Etat Lie</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpim_2.Modelpim_2Package#getSimpleState()
 * @model
 * @generated
 */
public interface SimpleState extends EObject {
	/**
	 * Returns the value of the '<em><b>Trans Out1</b></em>' containment reference list.
	 * The list contents are of type {@link modelpim_2.Edge}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trans Out1</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trans Out1</em>' containment reference list.
	 * @see modelpim_2.Modelpim_2Package#getSimpleState_TransOut1()
	 * @model type="modelpim_2.Edge" containment="true" required="true"
	 * @generated
	 */
	EList getTransOut1();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelpim_2.Modelpim_2Package#getSimpleState_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelpim_2.SimpleState#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abs</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abs</em>' attribute.
	 * @see #setAbs(BigInteger)
	 * @see modelpim_2.Modelpim_2Package#getSimpleState_Abs()
	 * @model
	 * @generated
	 */
	BigInteger getAbs();

	/**
	 * Sets the value of the '{@link modelpim_2.SimpleState#getAbs <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abs</em>' attribute.
	 * @see #getAbs()
	 * @generated
	 */
	void setAbs(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ord</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ord</em>' attribute.
	 * @see #setOrd(BigInteger)
	 * @see modelpim_2.Modelpim_2Package#getSimpleState_Ord()
	 * @model
	 * @generated
	 */
	BigInteger getOrd();

	/**
	 * Sets the value of the '{@link modelpim_2.SimpleState#getOrd <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ord</em>' attribute.
	 * @see #getOrd()
	 * @generated
	 */
	void setOrd(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Symbole</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbole</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbole</em>' attribute.
	 * @see #setSymbole(int)
	 * @see modelpim_2.Modelpim_2Package#getSimpleState_Symbole()
	 * @model
	 * @generated
	 */
	int getSymbole();

	/**
	 * Sets the value of the '{@link modelpim_2.SimpleState#getSymbole <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbole</em>' attribute.
	 * @see #getSymbole()
	 * @generated
	 */
	void setSymbole(int value);

	/**
	 * Returns the value of the '<em><b>Nb Etat Lie</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nb Etat Lie</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Etat Lie</em>' attribute.
	 * @see #setNbEtatLie(int)
	 * @see modelpim_2.Modelpim_2Package#getSimpleState_NbEtatLie()
	 * @model
	 * @generated
	 */
	int getNbEtatLie();

	/**
	 * Sets the value of the '{@link modelpim_2.SimpleState#getNbEtatLie <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Etat Lie</em>' attribute.
	 * @see #getNbEtatLie()
	 * @generated
	 */
	void setNbEtatLie(int value);

} // SimpleState
