/**
 */
package modelpim_2;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpim_2.Edge#getEtatCibleSimple <em>Etat Cible Simple</em>}</li>
 *   <li>{@link modelpim_2.Edge#getEtatCibleComposite <em>Etat Cible Composite</em>}</li>
 *   <li>{@link modelpim_2.Edge#getEvent <em>Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpim_2.Modelpim_2Package#getEdge()
 * @model
 * @generated
 */
public interface Edge extends EObject {
	/**
	 * Returns the value of the '<em><b>Etat Cible Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Etat Cible Simple</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Etat Cible Simple</em>' reference.
	 * @see #setEtatCibleSimple(SimpleState)
	 * @see modelpim_2.Modelpim_2Package#getEdge_EtatCibleSimple()
	 * @model required="true"
	 * @generated
	 */
	SimpleState getEtatCibleSimple();

	/**
	 * Sets the value of the '{@link modelpim_2.Edge#getEtatCibleSimple <em>Etat Cible Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Etat Cible Simple</em>' reference.
	 * @see #getEtatCibleSimple()
	 * @generated
	 */
	void setEtatCibleSimple(SimpleState value);

	/**
	 * Returns the value of the '<em><b>Etat Cible Composite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Etat Cible Composite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Etat Cible Composite</em>' reference.
	 * @see #setEtatCibleComposite(CompositeState)
	 * @see modelpim_2.Modelpim_2Package#getEdge_EtatCibleComposite()
	 * @model required="true"
	 * @generated
	 */
	CompositeState getEtatCibleComposite();

	/**
	 * Sets the value of the '{@link modelpim_2.Edge#getEtatCibleComposite <em>Etat Cible Composite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Etat Cible Composite</em>' reference.
	 * @see #getEtatCibleComposite()
	 * @generated
	 */
	void setEtatCibleComposite(CompositeState value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' attribute.
	 * @see #setEvent(String)
	 * @see modelpim_2.Modelpim_2Package#getEdge_Event()
	 * @model required="true"
	 * @generated
	 */
	String getEvent();

	/**
	 * Sets the value of the '{@link modelpim_2.Edge#getEvent <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' attribute.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(String value);

} // Edge
