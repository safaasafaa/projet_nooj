/**
 */
package modelpim_2;

import java.math.BigInteger;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Note</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpim_2.Note#getName <em>Name</em>}</li>
 *   <li>{@link modelpim_2.Note#getAbs <em>Abs</em>}</li>
 *   <li>{@link modelpim_2.Note#getOrd <em>Ord</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpim_2.Modelpim_2Package#getNote()
 * @model
 * @generated
 */
public interface Note extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelpim_2.Modelpim_2Package#getNote_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelpim_2.Note#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abs</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abs</em>' attribute.
	 * @see #setAbs(BigInteger)
	 * @see modelpim_2.Modelpim_2Package#getNote_Abs()
	 * @model
	 * @generated
	 */
	BigInteger getAbs();

	/**
	 * Sets the value of the '{@link modelpim_2.Note#getAbs <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abs</em>' attribute.
	 * @see #getAbs()
	 * @generated
	 */
	void setAbs(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ord</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ord</em>' attribute.
	 * @see #setOrd(BigInteger)
	 * @see modelpim_2.Modelpim_2Package#getNote_Ord()
	 * @model
	 * @generated
	 */
	BigInteger getOrd();

	/**
	 * Sets the value of the '{@link modelpim_2.Note#getOrd <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ord</em>' attribute.
	 * @see #getOrd()
	 * @generated
	 */
	void setOrd(BigInteger value);

} // Note
