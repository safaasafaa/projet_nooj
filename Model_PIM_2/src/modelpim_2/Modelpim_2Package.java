/**
 */
package modelpim_2;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see modelpim_2.Modelpim_2Factory
 * @model kind="package"
 * @generated
 */
public interface Modelpim_2Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "modelpim_2";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://modelpim_2/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "modelpim_2";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Modelpim_2Package eINSTANCE = modelpim_2.impl.Modelpim_2PackageImpl.init();

	/**
	 * The meta object id for the '{@link modelpim_2.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpim_2.impl.ModelImpl
	 * @see modelpim_2.impl.Modelpim_2PackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 0;

	/**
	 * The feature id for the '<em><b>Diags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__DIAGS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__NAME = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link modelpim_2.impl.DiagramImpl <em>Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpim_2.impl.DiagramImpl
	 * @see modelpim_2.impl.Modelpim_2PackageImpl#getDiagram()
	 * @generated
	 */
	int DIAGRAM = 1;

	/**
	 * The feature id for the '<em><b>Simple States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__SIMPLE_STATES = 0;

	/**
	 * The feature id for the '<em><b>Composite States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__COMPOSITE_STATES = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__NAME = 2;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__NOTES = 3;

	/**
	 * The number of structural features of the '<em>Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link modelpim_2.impl.CompositeStateImpl <em>Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpim_2.impl.CompositeStateImpl
	 * @see modelpim_2.impl.Modelpim_2PackageImpl#getCompositeState()
	 * @generated
	 */
	int COMPOSITE_STATE = 2;

	/**
	 * The feature id for the '<em><b>Cmp Diags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__CMP_DIAGS = 0;

	/**
	 * The feature id for the '<em><b>Trans Out2</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__TRANS_OUT2 = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__NAME = 2;

	/**
	 * The feature id for the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__ABS = 3;

	/**
	 * The feature id for the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__ORD = 4;

	/**
	 * The feature id for the '<em><b>Symbole</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__SYMBOLE = 5;

	/**
	 * The feature id for the '<em><b>Nb Etat Lie</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE__NB_ETAT_LIE = 6;

	/**
	 * The number of structural features of the '<em>Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STATE_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link modelpim_2.impl.SimpleStateImpl <em>Simple State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpim_2.impl.SimpleStateImpl
	 * @see modelpim_2.impl.Modelpim_2PackageImpl#getSimpleState()
	 * @generated
	 */
	int SIMPLE_STATE = 3;

	/**
	 * The feature id for the '<em><b>Trans Out1</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__TRANS_OUT1 = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__ABS = 2;

	/**
	 * The feature id for the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__ORD = 3;

	/**
	 * The feature id for the '<em><b>Symbole</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__SYMBOLE = 4;

	/**
	 * The feature id for the '<em><b>Nb Etat Lie</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE__NB_ETAT_LIE = 5;

	/**
	 * The number of structural features of the '<em>Simple State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STATE_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link modelpim_2.impl.EdgeImpl <em>Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpim_2.impl.EdgeImpl
	 * @see modelpim_2.impl.Modelpim_2PackageImpl#getEdge()
	 * @generated
	 */
	int EDGE = 4;

	/**
	 * The feature id for the '<em><b>Etat Cible Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__ETAT_CIBLE_SIMPLE = 0;

	/**
	 * The feature id for the '<em><b>Etat Cible Composite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__ETAT_CIBLE_COMPOSITE = 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE__EVENT = 2;

	/**
	 * The number of structural features of the '<em>Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link modelpim_2.impl.NoteImpl <em>Note</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpim_2.impl.NoteImpl
	 * @see modelpim_2.impl.Modelpim_2PackageImpl#getNote()
	 * @generated
	 */
	int NOTE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__ABS = 1;

	/**
	 * The feature id for the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE__ORD = 2;

	/**
	 * The number of structural features of the '<em>Note</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTE_FEATURE_COUNT = 3;

	/**
	 * Returns the meta object for class '{@link modelpim_2.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see modelpim_2.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpim_2.Model#getDiags <em>Diags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Diags</em>'.
	 * @see modelpim_2.Model#getDiags()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Diags();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.Model#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpim_2.Model#getName()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_Name();

	/**
	 * Returns the meta object for class '{@link modelpim_2.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagram</em>'.
	 * @see modelpim_2.Diagram
	 * @generated
	 */
	EClass getDiagram();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpim_2.Diagram#getSimpleStates <em>Simple States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Simple States</em>'.
	 * @see modelpim_2.Diagram#getSimpleStates()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_SimpleStates();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpim_2.Diagram#getCompositeStates <em>Composite States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Composite States</em>'.
	 * @see modelpim_2.Diagram#getCompositeStates()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_CompositeStates();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.Diagram#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpim_2.Diagram#getName()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpim_2.Diagram#getNotes <em>Notes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Notes</em>'.
	 * @see modelpim_2.Diagram#getNotes()
	 * @see #getDiagram()
	 * @generated
	 */
	EReference getDiagram_Notes();

	/**
	 * Returns the meta object for class '{@link modelpim_2.CompositeState <em>Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite State</em>'.
	 * @see modelpim_2.CompositeState
	 * @generated
	 */
	EClass getCompositeState();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpim_2.CompositeState#getCmpDiags <em>Cmp Diags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cmp Diags</em>'.
	 * @see modelpim_2.CompositeState#getCmpDiags()
	 * @see #getCompositeState()
	 * @generated
	 */
	EReference getCompositeState_CmpDiags();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpim_2.CompositeState#getTransOut2 <em>Trans Out2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trans Out2</em>'.
	 * @see modelpim_2.CompositeState#getTransOut2()
	 * @see #getCompositeState()
	 * @generated
	 */
	EReference getCompositeState_TransOut2();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.CompositeState#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpim_2.CompositeState#getName()
	 * @see #getCompositeState()
	 * @generated
	 */
	EAttribute getCompositeState_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.CompositeState#getAbs <em>Abs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abs</em>'.
	 * @see modelpim_2.CompositeState#getAbs()
	 * @see #getCompositeState()
	 * @generated
	 */
	EAttribute getCompositeState_Abs();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.CompositeState#getOrd <em>Ord</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ord</em>'.
	 * @see modelpim_2.CompositeState#getOrd()
	 * @see #getCompositeState()
	 * @generated
	 */
	EAttribute getCompositeState_Ord();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.CompositeState#getSymbole <em>Symbole</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbole</em>'.
	 * @see modelpim_2.CompositeState#getSymbole()
	 * @see #getCompositeState()
	 * @generated
	 */
	EAttribute getCompositeState_Symbole();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.CompositeState#getNbEtatLie <em>Nb Etat Lie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Etat Lie</em>'.
	 * @see modelpim_2.CompositeState#getNbEtatLie()
	 * @see #getCompositeState()
	 * @generated
	 */
	EAttribute getCompositeState_NbEtatLie();

	/**
	 * Returns the meta object for class '{@link modelpim_2.SimpleState <em>Simple State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple State</em>'.
	 * @see modelpim_2.SimpleState
	 * @generated
	 */
	EClass getSimpleState();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpim_2.SimpleState#getTransOut1 <em>Trans Out1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trans Out1</em>'.
	 * @see modelpim_2.SimpleState#getTransOut1()
	 * @see #getSimpleState()
	 * @generated
	 */
	EReference getSimpleState_TransOut1();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.SimpleState#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpim_2.SimpleState#getName()
	 * @see #getSimpleState()
	 * @generated
	 */
	EAttribute getSimpleState_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.SimpleState#getAbs <em>Abs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abs</em>'.
	 * @see modelpim_2.SimpleState#getAbs()
	 * @see #getSimpleState()
	 * @generated
	 */
	EAttribute getSimpleState_Abs();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.SimpleState#getOrd <em>Ord</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ord</em>'.
	 * @see modelpim_2.SimpleState#getOrd()
	 * @see #getSimpleState()
	 * @generated
	 */
	EAttribute getSimpleState_Ord();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.SimpleState#getSymbole <em>Symbole</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbole</em>'.
	 * @see modelpim_2.SimpleState#getSymbole()
	 * @see #getSimpleState()
	 * @generated
	 */
	EAttribute getSimpleState_Symbole();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.SimpleState#getNbEtatLie <em>Nb Etat Lie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Etat Lie</em>'.
	 * @see modelpim_2.SimpleState#getNbEtatLie()
	 * @see #getSimpleState()
	 * @generated
	 */
	EAttribute getSimpleState_NbEtatLie();

	/**
	 * Returns the meta object for class '{@link modelpim_2.Edge <em>Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge</em>'.
	 * @see modelpim_2.Edge
	 * @generated
	 */
	EClass getEdge();

	/**
	 * Returns the meta object for the reference '{@link modelpim_2.Edge#getEtatCibleSimple <em>Etat Cible Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Etat Cible Simple</em>'.
	 * @see modelpim_2.Edge#getEtatCibleSimple()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_EtatCibleSimple();

	/**
	 * Returns the meta object for the reference '{@link modelpim_2.Edge#getEtatCibleComposite <em>Etat Cible Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Etat Cible Composite</em>'.
	 * @see modelpim_2.Edge#getEtatCibleComposite()
	 * @see #getEdge()
	 * @generated
	 */
	EReference getEdge_EtatCibleComposite();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.Edge#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event</em>'.
	 * @see modelpim_2.Edge#getEvent()
	 * @see #getEdge()
	 * @generated
	 */
	EAttribute getEdge_Event();

	/**
	 * Returns the meta object for class '{@link modelpim_2.Note <em>Note</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Note</em>'.
	 * @see modelpim_2.Note
	 * @generated
	 */
	EClass getNote();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.Note#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpim_2.Note#getName()
	 * @see #getNote()
	 * @generated
	 */
	EAttribute getNote_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.Note#getAbs <em>Abs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abs</em>'.
	 * @see modelpim_2.Note#getAbs()
	 * @see #getNote()
	 * @generated
	 */
	EAttribute getNote_Abs();

	/**
	 * Returns the meta object for the attribute '{@link modelpim_2.Note#getOrd <em>Ord</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ord</em>'.
	 * @see modelpim_2.Note#getOrd()
	 * @see #getNote()
	 * @generated
	 */
	EAttribute getNote_Ord();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Modelpim_2Factory getModelpim_2Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link modelpim_2.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpim_2.impl.ModelImpl
		 * @see modelpim_2.impl.Modelpim_2PackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>Diags</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__DIAGS = eINSTANCE.getModel_Diags();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__NAME = eINSTANCE.getModel_Name();

		/**
		 * The meta object literal for the '{@link modelpim_2.impl.DiagramImpl <em>Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpim_2.impl.DiagramImpl
		 * @see modelpim_2.impl.Modelpim_2PackageImpl#getDiagram()
		 * @generated
		 */
		EClass DIAGRAM = eINSTANCE.getDiagram();

		/**
		 * The meta object literal for the '<em><b>Simple States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIAGRAM__SIMPLE_STATES = eINSTANCE.getDiagram_SimpleStates();

		/**
		 * The meta object literal for the '<em><b>Composite States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIAGRAM__COMPOSITE_STATES = eINSTANCE.getDiagram_CompositeStates();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__NAME = eINSTANCE.getDiagram_Name();

		/**
		 * The meta object literal for the '<em><b>Notes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIAGRAM__NOTES = eINSTANCE.getDiagram_Notes();

		/**
		 * The meta object literal for the '{@link modelpim_2.impl.CompositeStateImpl <em>Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpim_2.impl.CompositeStateImpl
		 * @see modelpim_2.impl.Modelpim_2PackageImpl#getCompositeState()
		 * @generated
		 */
		EClass COMPOSITE_STATE = eINSTANCE.getCompositeState();

		/**
		 * The meta object literal for the '<em><b>Cmp Diags</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_STATE__CMP_DIAGS = eINSTANCE.getCompositeState_CmpDiags();

		/**
		 * The meta object literal for the '<em><b>Trans Out2</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_STATE__TRANS_OUT2 = eINSTANCE.getCompositeState_TransOut2();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_STATE__NAME = eINSTANCE.getCompositeState_Name();

		/**
		 * The meta object literal for the '<em><b>Abs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_STATE__ABS = eINSTANCE.getCompositeState_Abs();

		/**
		 * The meta object literal for the '<em><b>Ord</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_STATE__ORD = eINSTANCE.getCompositeState_Ord();

		/**
		 * The meta object literal for the '<em><b>Symbole</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_STATE__SYMBOLE = eINSTANCE.getCompositeState_Symbole();

		/**
		 * The meta object literal for the '<em><b>Nb Etat Lie</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_STATE__NB_ETAT_LIE = eINSTANCE.getCompositeState_NbEtatLie();

		/**
		 * The meta object literal for the '{@link modelpim_2.impl.SimpleStateImpl <em>Simple State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpim_2.impl.SimpleStateImpl
		 * @see modelpim_2.impl.Modelpim_2PackageImpl#getSimpleState()
		 * @generated
		 */
		EClass SIMPLE_STATE = eINSTANCE.getSimpleState();

		/**
		 * The meta object literal for the '<em><b>Trans Out1</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_STATE__TRANS_OUT1 = eINSTANCE.getSimpleState_TransOut1();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_STATE__NAME = eINSTANCE.getSimpleState_Name();

		/**
		 * The meta object literal for the '<em><b>Abs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_STATE__ABS = eINSTANCE.getSimpleState_Abs();

		/**
		 * The meta object literal for the '<em><b>Ord</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_STATE__ORD = eINSTANCE.getSimpleState_Ord();

		/**
		 * The meta object literal for the '<em><b>Symbole</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_STATE__SYMBOLE = eINSTANCE.getSimpleState_Symbole();

		/**
		 * The meta object literal for the '<em><b>Nb Etat Lie</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_STATE__NB_ETAT_LIE = eINSTANCE.getSimpleState_NbEtatLie();

		/**
		 * The meta object literal for the '{@link modelpim_2.impl.EdgeImpl <em>Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpim_2.impl.EdgeImpl
		 * @see modelpim_2.impl.Modelpim_2PackageImpl#getEdge()
		 * @generated
		 */
		EClass EDGE = eINSTANCE.getEdge();

		/**
		 * The meta object literal for the '<em><b>Etat Cible Simple</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__ETAT_CIBLE_SIMPLE = eINSTANCE.getEdge_EtatCibleSimple();

		/**
		 * The meta object literal for the '<em><b>Etat Cible Composite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EDGE__ETAT_CIBLE_COMPOSITE = eINSTANCE.getEdge_EtatCibleComposite();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE__EVENT = eINSTANCE.getEdge_Event();

		/**
		 * The meta object literal for the '{@link modelpim_2.impl.NoteImpl <em>Note</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpim_2.impl.NoteImpl
		 * @see modelpim_2.impl.Modelpim_2PackageImpl#getNote()
		 * @generated
		 */
		EClass NOTE = eINSTANCE.getNote();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOTE__NAME = eINSTANCE.getNote_Name();

		/**
		 * The meta object literal for the '<em><b>Abs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOTE__ABS = eINSTANCE.getNote_Abs();

		/**
		 * The meta object literal for the '<em><b>Ord</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOTE__ORD = eINSTANCE.getNote_Ord();

	}

} //Modelpim_2Package
