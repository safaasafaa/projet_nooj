/**
 */
package modelpsm_2.impl;

import java.util.Collection;
import modelpsm_2.Commentaire;
import modelpsm_2.Graphe;
import modelpsm_2.Modelpsm_2Package;
import modelpsm_2.NoeudComposite;
import modelpsm_2.NoeudSimple;
import modelpsm_2.Transition;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Graphe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelpsm_2.impl.GrapheImpl#getNoeuds <em>Noeuds</em>}</li>
 *   <li>{@link modelpsm_2.impl.GrapheImpl#getAssociations <em>Associations</em>}</li>
 *   <li>{@link modelpsm_2.impl.GrapheImpl#getNoeudsComposite <em>Noeuds Composite</em>}</li>
 *   <li>{@link modelpsm_2.impl.GrapheImpl#getName <em>Name</em>}</li>
 *   <li>{@link modelpsm_2.impl.GrapheImpl#getComments <em>Comments</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GrapheImpl extends MinimalEObjectImpl.Container implements Graphe {
	/**
	 * The cached value of the '{@link #getNoeuds() <em>Noeuds</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoeuds()
	 * @generated
	 * @ordered
	 */
	protected EList noeuds;

	/**
	 * The cached value of the '{@link #getAssociations() <em>Associations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociations()
	 * @generated
	 * @ordered
	 */
	protected EList associations;

	/**
	 * The cached value of the '{@link #getNoeudsComposite() <em>Noeuds Composite</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoeudsComposite()
	 * @generated
	 * @ordered
	 */
	protected EList noeudsComposite;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComments() <em>Comments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComments()
	 * @generated
	 * @ordered
	 */
	protected EList comments;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GrapheImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Modelpsm_2Package.Literals.GRAPHE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getNoeuds() {
		if (noeuds == null) {
			noeuds = new EObjectContainmentEList(NoeudSimple.class, this, Modelpsm_2Package.GRAPHE__NOEUDS);
		}
		return noeuds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getAssociations() {
		if (associations == null) {
			associations = new EObjectContainmentEList(Transition.class, this, Modelpsm_2Package.GRAPHE__ASSOCIATIONS);
		}
		return associations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getNoeudsComposite() {
		if (noeudsComposite == null) {
			noeudsComposite = new EObjectContainmentEList(NoeudComposite.class, this, Modelpsm_2Package.GRAPHE__NOEUDS_COMPOSITE);
		}
		return noeudsComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.GRAPHE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getComments() {
		if (comments == null) {
			comments = new EObjectContainmentEList(Commentaire.class, this, Modelpsm_2Package.GRAPHE__COMMENTS);
		}
		return comments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Modelpsm_2Package.GRAPHE__NOEUDS:
				return ((InternalEList)getNoeuds()).basicRemove(otherEnd, msgs);
			case Modelpsm_2Package.GRAPHE__ASSOCIATIONS:
				return ((InternalEList)getAssociations()).basicRemove(otherEnd, msgs);
			case Modelpsm_2Package.GRAPHE__NOEUDS_COMPOSITE:
				return ((InternalEList)getNoeudsComposite()).basicRemove(otherEnd, msgs);
			case Modelpsm_2Package.GRAPHE__COMMENTS:
				return ((InternalEList)getComments()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Modelpsm_2Package.GRAPHE__NOEUDS:
				return getNoeuds();
			case Modelpsm_2Package.GRAPHE__ASSOCIATIONS:
				return getAssociations();
			case Modelpsm_2Package.GRAPHE__NOEUDS_COMPOSITE:
				return getNoeudsComposite();
			case Modelpsm_2Package.GRAPHE__NAME:
				return getName();
			case Modelpsm_2Package.GRAPHE__COMMENTS:
				return getComments();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Modelpsm_2Package.GRAPHE__NOEUDS:
				getNoeuds().clear();
				getNoeuds().addAll((Collection)newValue);
				return;
			case Modelpsm_2Package.GRAPHE__ASSOCIATIONS:
				getAssociations().clear();
				getAssociations().addAll((Collection)newValue);
				return;
			case Modelpsm_2Package.GRAPHE__NOEUDS_COMPOSITE:
				getNoeudsComposite().clear();
				getNoeudsComposite().addAll((Collection)newValue);
				return;
			case Modelpsm_2Package.GRAPHE__NAME:
				setName((String)newValue);
				return;
			case Modelpsm_2Package.GRAPHE__COMMENTS:
				getComments().clear();
				getComments().addAll((Collection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.GRAPHE__NOEUDS:
				getNoeuds().clear();
				return;
			case Modelpsm_2Package.GRAPHE__ASSOCIATIONS:
				getAssociations().clear();
				return;
			case Modelpsm_2Package.GRAPHE__NOEUDS_COMPOSITE:
				getNoeudsComposite().clear();
				return;
			case Modelpsm_2Package.GRAPHE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Modelpsm_2Package.GRAPHE__COMMENTS:
				getComments().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.GRAPHE__NOEUDS:
				return noeuds != null && !noeuds.isEmpty();
			case Modelpsm_2Package.GRAPHE__ASSOCIATIONS:
				return associations != null && !associations.isEmpty();
			case Modelpsm_2Package.GRAPHE__NOEUDS_COMPOSITE:
				return noeudsComposite != null && !noeudsComposite.isEmpty();
			case Modelpsm_2Package.GRAPHE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Modelpsm_2Package.GRAPHE__COMMENTS:
				return comments != null && !comments.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //GrapheImpl
