/**
 */
package modelpsm_2.impl;

import java.math.BigInteger;
import java.util.Collection;

import modelpsm_2.Graphe;
import modelpsm_2.Modelpsm_2Package;
import modelpsm_2.NoeudComposite;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Noeud Composite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelpsm_2.impl.NoeudCompositeImpl#getCmpGraphes <em>Cmp Graphes</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudCompositeImpl#getName <em>Name</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudCompositeImpl#getAbs <em>Abs</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudCompositeImpl#getOrd <em>Ord</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudCompositeImpl#getNbEtatLie <em>Nb Etat Lie</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudCompositeImpl#getSymbole <em>Symbole</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NoeudCompositeImpl extends MinimalEObjectImpl.Container implements NoeudComposite {
	/**
	 * The cached value of the '{@link #getCmpGraphes() <em>Cmp Graphes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCmpGraphes()
	 * @generated
	 * @ordered
	 */
	protected EList cmpGraphes;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getAbs() <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbs()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ABS_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getAbs() <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbs()
	 * @generated
	 * @ordered
	 */
	protected BigInteger abs = ABS_EDEFAULT;
	/**
	 * The default value of the '{@link #getOrd() <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrd()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ORD_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getOrd() <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrd()
	 * @generated
	 * @ordered
	 */
	protected BigInteger ord = ORD_EDEFAULT;
	/**
	 * The default value of the '{@link #getNbEtatLie() <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbEtatLie()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_ETAT_LIE_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getNbEtatLie() <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbEtatLie()
	 * @generated
	 * @ordered
	 */
	protected int nbEtatLie = NB_ETAT_LIE_EDEFAULT;
	/**
	 * The default value of the '{@link #getSymbole() <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbole()
	 * @generated
	 * @ordered
	 */
	protected static final int SYMBOLE_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getSymbole() <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbole()
	 * @generated
	 * @ordered
	 */
	protected int symbole = SYMBOLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NoeudCompositeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Modelpsm_2Package.Literals.NOEUD_COMPOSITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList getCmpGraphes() {
		if (cmpGraphes == null) {
			cmpGraphes = new EObjectContainmentEList(Graphe.class, this, Modelpsm_2Package.NOEUD_COMPOSITE__CMP_GRAPHES);
		}
		return cmpGraphes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_COMPOSITE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getAbs() {
		return abs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbs(BigInteger newAbs) {
		BigInteger oldAbs = abs;
		abs = newAbs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_COMPOSITE__ABS, oldAbs, abs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getOrd() {
		return ord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrd(BigInteger newOrd) {
		BigInteger oldOrd = ord;
		ord = newOrd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_COMPOSITE__ORD, oldOrd, ord));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbEtatLie() {
		return nbEtatLie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNbEtatLie(int newNbEtatLie) {
		int oldNbEtatLie = nbEtatLie;
		nbEtatLie = newNbEtatLie;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_COMPOSITE__NB_ETAT_LIE, oldNbEtatLie, nbEtatLie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSymbole() {
		return symbole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbole(int newSymbole) {
		int oldSymbole = symbole;
		symbole = newSymbole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_COMPOSITE__SYMBOLE, oldSymbole, symbole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_COMPOSITE__CMP_GRAPHES:
				return ((InternalEList)getCmpGraphes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_COMPOSITE__CMP_GRAPHES:
				return getCmpGraphes();
			case Modelpsm_2Package.NOEUD_COMPOSITE__NAME:
				return getName();
			case Modelpsm_2Package.NOEUD_COMPOSITE__ABS:
				return getAbs();
			case Modelpsm_2Package.NOEUD_COMPOSITE__ORD:
				return getOrd();
			case Modelpsm_2Package.NOEUD_COMPOSITE__NB_ETAT_LIE:
				return new Integer(getNbEtatLie());
			case Modelpsm_2Package.NOEUD_COMPOSITE__SYMBOLE:
				return new Integer(getSymbole());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_COMPOSITE__CMP_GRAPHES:
				getCmpGraphes().clear();
				getCmpGraphes().addAll((Collection)newValue);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__NAME:
				setName((String)newValue);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__ABS:
				setAbs((BigInteger)newValue);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__ORD:
				setOrd((BigInteger)newValue);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__NB_ETAT_LIE:
				setNbEtatLie(((Integer)newValue).intValue());
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__SYMBOLE:
				setSymbole(((Integer)newValue).intValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_COMPOSITE__CMP_GRAPHES:
				getCmpGraphes().clear();
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__ABS:
				setAbs(ABS_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__ORD:
				setOrd(ORD_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__NB_ETAT_LIE:
				setNbEtatLie(NB_ETAT_LIE_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_COMPOSITE__SYMBOLE:
				setSymbole(SYMBOLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_COMPOSITE__CMP_GRAPHES:
				return cmpGraphes != null && !cmpGraphes.isEmpty();
			case Modelpsm_2Package.NOEUD_COMPOSITE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Modelpsm_2Package.NOEUD_COMPOSITE__ABS:
				return ABS_EDEFAULT == null ? abs != null : !ABS_EDEFAULT.equals(abs);
			case Modelpsm_2Package.NOEUD_COMPOSITE__ORD:
				return ORD_EDEFAULT == null ? ord != null : !ORD_EDEFAULT.equals(ord);
			case Modelpsm_2Package.NOEUD_COMPOSITE__NB_ETAT_LIE:
				return nbEtatLie != NB_ETAT_LIE_EDEFAULT;
			case Modelpsm_2Package.NOEUD_COMPOSITE__SYMBOLE:
				return symbole != SYMBOLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", abs: ");
		result.append(abs);
		result.append(", ord: ");
		result.append(ord);
		result.append(", nbEtatLie: ");
		result.append(nbEtatLie);
		result.append(", symbole: ");
		result.append(symbole);
		result.append(')');
		return result.toString();
	}

} //NoeudCompositeImpl
