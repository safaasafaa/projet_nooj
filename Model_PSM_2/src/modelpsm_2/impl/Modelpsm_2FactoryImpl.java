/**
 */
package modelpsm_2.impl;

import modelpsm_2.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Modelpsm_2FactoryImpl extends EFactoryImpl implements Modelpsm_2Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Modelpsm_2Factory init() {
		try {
			Modelpsm_2Factory theModelpsm_2Factory = (Modelpsm_2Factory)EPackage.Registry.INSTANCE.getEFactory(Modelpsm_2Package.eNS_URI);
			if (theModelpsm_2Factory != null) {
				return theModelpsm_2Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Modelpsm_2FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modelpsm_2FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Modelpsm_2Package.GRAMMAIRE: return createGrammaire();
			case Modelpsm_2Package.GRAPHE: return createGraphe();
			case Modelpsm_2Package.NOEUD_COMPOSITE: return createNoeudComposite();
			case Modelpsm_2Package.NOEUD_SIMPLE: return createNoeudSimple();
			case Modelpsm_2Package.TRANSITION: return createTransition();
			case Modelpsm_2Package.COMMENTAIRE: return createCommentaire();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Grammaire createGrammaire() {
		GrammaireImpl grammaire = new GrammaireImpl();
		return grammaire;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Graphe createGraphe() {
		GrapheImpl graphe = new GrapheImpl();
		return graphe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudComposite createNoeudComposite() {
		NoeudCompositeImpl noeudComposite = new NoeudCompositeImpl();
		return noeudComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudSimple createNoeudSimple() {
		NoeudSimpleImpl noeudSimple = new NoeudSimpleImpl();
		return noeudSimple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Commentaire createCommentaire() {
		CommentaireImpl commentaire = new CommentaireImpl();
		return commentaire;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modelpsm_2Package getModelpsm_2Package() {
		return (Modelpsm_2Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static Modelpsm_2Package getPackage() {
		return Modelpsm_2Package.eINSTANCE;
	}

} //Modelpsm_2FactoryImpl
