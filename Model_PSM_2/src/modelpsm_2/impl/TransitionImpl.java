/**
 */
package modelpsm_2.impl;

import modelpsm_2.Modelpsm_2Package;
import modelpsm_2.NoeudComposite;
import modelpsm_2.NoeudSimple;
import modelpsm_2.Transition;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelpsm_2.impl.TransitionImpl#getStateInNoeudSimple <em>State In Noeud Simple</em>}</li>
 *   <li>{@link modelpsm_2.impl.TransitionImpl#getStateOutNoeudSimple <em>State Out Noeud Simple</em>}</li>
 *   <li>{@link modelpsm_2.impl.TransitionImpl#getStateInComposite <em>State In Composite</em>}</li>
 *   <li>{@link modelpsm_2.impl.TransitionImpl#getStateOutComposite <em>State Out Composite</em>}</li>
 *   <li>{@link modelpsm_2.impl.TransitionImpl#getEvent <em>Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TransitionImpl extends MinimalEObjectImpl.Container implements Transition {
	/**
	 * The cached value of the '{@link #getStateInNoeudSimple() <em>State In Noeud Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateInNoeudSimple()
	 * @generated
	 * @ordered
	 */
	protected NoeudSimple stateInNoeudSimple;

	/**
	 * The cached value of the '{@link #getStateOutNoeudSimple() <em>State Out Noeud Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateOutNoeudSimple()
	 * @generated
	 * @ordered
	 */
	protected NoeudSimple stateOutNoeudSimple;

	/**
	 * The cached value of the '{@link #getStateInComposite() <em>State In Composite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateInComposite()
	 * @generated
	 * @ordered
	 */
	protected NoeudComposite stateInComposite;

	/**
	 * The cached value of the '{@link #getStateOutComposite() <em>State Out Composite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateOutComposite()
	 * @generated
	 * @ordered
	 */
	protected NoeudComposite stateOutComposite;

	/**
	 * The default value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected String event = EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Modelpsm_2Package.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudSimple getStateInNoeudSimple() {
		if (stateInNoeudSimple != null && stateInNoeudSimple.eIsProxy()) {
			InternalEObject oldStateInNoeudSimple = (InternalEObject)stateInNoeudSimple;
			stateInNoeudSimple = (NoeudSimple)eResolveProxy(oldStateInNoeudSimple);
			if (stateInNoeudSimple != oldStateInNoeudSimple) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Modelpsm_2Package.TRANSITION__STATE_IN_NOEUD_SIMPLE, oldStateInNoeudSimple, stateInNoeudSimple));
			}
		}
		return stateInNoeudSimple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudSimple basicGetStateInNoeudSimple() {
		return stateInNoeudSimple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateInNoeudSimple(NoeudSimple newStateInNoeudSimple) {
		NoeudSimple oldStateInNoeudSimple = stateInNoeudSimple;
		stateInNoeudSimple = newStateInNoeudSimple;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.TRANSITION__STATE_IN_NOEUD_SIMPLE, oldStateInNoeudSimple, stateInNoeudSimple));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudSimple getStateOutNoeudSimple() {
		if (stateOutNoeudSimple != null && stateOutNoeudSimple.eIsProxy()) {
			InternalEObject oldStateOutNoeudSimple = (InternalEObject)stateOutNoeudSimple;
			stateOutNoeudSimple = (NoeudSimple)eResolveProxy(oldStateOutNoeudSimple);
			if (stateOutNoeudSimple != oldStateOutNoeudSimple) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Modelpsm_2Package.TRANSITION__STATE_OUT_NOEUD_SIMPLE, oldStateOutNoeudSimple, stateOutNoeudSimple));
			}
		}
		return stateOutNoeudSimple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudSimple basicGetStateOutNoeudSimple() {
		return stateOutNoeudSimple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateOutNoeudSimple(NoeudSimple newStateOutNoeudSimple) {
		NoeudSimple oldStateOutNoeudSimple = stateOutNoeudSimple;
		stateOutNoeudSimple = newStateOutNoeudSimple;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.TRANSITION__STATE_OUT_NOEUD_SIMPLE, oldStateOutNoeudSimple, stateOutNoeudSimple));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudComposite getStateInComposite() {
		if (stateInComposite != null && stateInComposite.eIsProxy()) {
			InternalEObject oldStateInComposite = (InternalEObject)stateInComposite;
			stateInComposite = (NoeudComposite)eResolveProxy(oldStateInComposite);
			if (stateInComposite != oldStateInComposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Modelpsm_2Package.TRANSITION__STATE_IN_COMPOSITE, oldStateInComposite, stateInComposite));
			}
		}
		return stateInComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudComposite basicGetStateInComposite() {
		return stateInComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateInComposite(NoeudComposite newStateInComposite) {
		NoeudComposite oldStateInComposite = stateInComposite;
		stateInComposite = newStateInComposite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.TRANSITION__STATE_IN_COMPOSITE, oldStateInComposite, stateInComposite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudComposite getStateOutComposite() {
		if (stateOutComposite != null && stateOutComposite.eIsProxy()) {
			InternalEObject oldStateOutComposite = (InternalEObject)stateOutComposite;
			stateOutComposite = (NoeudComposite)eResolveProxy(oldStateOutComposite);
			if (stateOutComposite != oldStateOutComposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Modelpsm_2Package.TRANSITION__STATE_OUT_COMPOSITE, oldStateOutComposite, stateOutComposite));
			}
		}
		return stateOutComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoeudComposite basicGetStateOutComposite() {
		return stateOutComposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStateOutComposite(NoeudComposite newStateOutComposite) {
		NoeudComposite oldStateOutComposite = stateOutComposite;
		stateOutComposite = newStateOutComposite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.TRANSITION__STATE_OUT_COMPOSITE, oldStateOutComposite, stateOutComposite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(String newEvent) {
		String oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.TRANSITION__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Modelpsm_2Package.TRANSITION__STATE_IN_NOEUD_SIMPLE:
				if (resolve) return getStateInNoeudSimple();
				return basicGetStateInNoeudSimple();
			case Modelpsm_2Package.TRANSITION__STATE_OUT_NOEUD_SIMPLE:
				if (resolve) return getStateOutNoeudSimple();
				return basicGetStateOutNoeudSimple();
			case Modelpsm_2Package.TRANSITION__STATE_IN_COMPOSITE:
				if (resolve) return getStateInComposite();
				return basicGetStateInComposite();
			case Modelpsm_2Package.TRANSITION__STATE_OUT_COMPOSITE:
				if (resolve) return getStateOutComposite();
				return basicGetStateOutComposite();
			case Modelpsm_2Package.TRANSITION__EVENT:
				return getEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Modelpsm_2Package.TRANSITION__STATE_IN_NOEUD_SIMPLE:
				setStateInNoeudSimple((NoeudSimple)newValue);
				return;
			case Modelpsm_2Package.TRANSITION__STATE_OUT_NOEUD_SIMPLE:
				setStateOutNoeudSimple((NoeudSimple)newValue);
				return;
			case Modelpsm_2Package.TRANSITION__STATE_IN_COMPOSITE:
				setStateInComposite((NoeudComposite)newValue);
				return;
			case Modelpsm_2Package.TRANSITION__STATE_OUT_COMPOSITE:
				setStateOutComposite((NoeudComposite)newValue);
				return;
			case Modelpsm_2Package.TRANSITION__EVENT:
				setEvent((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.TRANSITION__STATE_IN_NOEUD_SIMPLE:
				setStateInNoeudSimple((NoeudSimple)null);
				return;
			case Modelpsm_2Package.TRANSITION__STATE_OUT_NOEUD_SIMPLE:
				setStateOutNoeudSimple((NoeudSimple)null);
				return;
			case Modelpsm_2Package.TRANSITION__STATE_IN_COMPOSITE:
				setStateInComposite((NoeudComposite)null);
				return;
			case Modelpsm_2Package.TRANSITION__STATE_OUT_COMPOSITE:
				setStateOutComposite((NoeudComposite)null);
				return;
			case Modelpsm_2Package.TRANSITION__EVENT:
				setEvent(EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.TRANSITION__STATE_IN_NOEUD_SIMPLE:
				return stateInNoeudSimple != null;
			case Modelpsm_2Package.TRANSITION__STATE_OUT_NOEUD_SIMPLE:
				return stateOutNoeudSimple != null;
			case Modelpsm_2Package.TRANSITION__STATE_IN_COMPOSITE:
				return stateInComposite != null;
			case Modelpsm_2Package.TRANSITION__STATE_OUT_COMPOSITE:
				return stateOutComposite != null;
			case Modelpsm_2Package.TRANSITION__EVENT:
				return EVENT_EDEFAULT == null ? event != null : !EVENT_EDEFAULT.equals(event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (event: ");
		result.append(event);
		result.append(')');
		return result.toString();
	}

} //TransitionImpl
