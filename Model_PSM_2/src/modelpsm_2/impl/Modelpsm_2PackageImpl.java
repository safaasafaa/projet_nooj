/**
 */
package modelpsm_2.impl;

import modelpsm_2.Commentaire;
import modelpsm_2.Grammaire;
import modelpsm_2.Graphe;
import modelpsm_2.Modelpsm_2Factory;
import modelpsm_2.Modelpsm_2Package;
import modelpsm_2.NoeudComposite;
import modelpsm_2.NoeudSimple;
import modelpsm_2.Transition;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Modelpsm_2PackageImpl extends EPackageImpl implements Modelpsm_2Package {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass grammaireEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass grapheEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass noeudCompositeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass noeudSimpleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commentaireEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see modelpsm_2.Modelpsm_2Package#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Modelpsm_2PackageImpl() {
		super(eNS_URI, Modelpsm_2Factory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link Modelpsm_2Package#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Modelpsm_2Package init() {
		if (isInited) return (Modelpsm_2Package)EPackage.Registry.INSTANCE.getEPackage(Modelpsm_2Package.eNS_URI);

		// Obtain or create and register package
		Modelpsm_2PackageImpl theModelpsm_2Package = (Modelpsm_2PackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof Modelpsm_2PackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new Modelpsm_2PackageImpl());

		isInited = true;

		// Create package meta-data objects
		theModelpsm_2Package.createPackageContents();

		// Initialize created meta-data
		theModelpsm_2Package.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theModelpsm_2Package.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Modelpsm_2Package.eNS_URI, theModelpsm_2Package);
		return theModelpsm_2Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGrammaire() {
		return grammaireEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGrammaire_Graphes() {
		return (EReference)grammaireEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGrammaire_Name() {
		return (EAttribute)grammaireEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGraphe() {
		return grapheEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphe_Noeuds() {
		return (EReference)grapheEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphe_Associations() {
		return (EReference)grapheEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphe_NoeudsComposite() {
		return (EReference)grapheEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGraphe_Name() {
		return (EAttribute)grapheEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGraphe_Comments() {
		return (EReference)grapheEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNoeudComposite() {
		return noeudCompositeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNoeudComposite_CmpGraphes() {
		return (EReference)noeudCompositeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudComposite_Name() {
		return (EAttribute)noeudCompositeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudComposite_Abs() {
		return (EAttribute)noeudCompositeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudComposite_Ord() {
		return (EAttribute)noeudCompositeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudComposite_NbEtatLie() {
		return (EAttribute)noeudCompositeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudComposite_Symbole() {
		return (EAttribute)noeudCompositeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNoeudSimple() {
		return noeudSimpleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudSimple_Name() {
		return (EAttribute)noeudSimpleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudSimple_Abs() {
		return (EAttribute)noeudSimpleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudSimple_Ord() {
		return (EAttribute)noeudSimpleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudSimple_NbEtatLie() {
		return (EAttribute)noeudSimpleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNoeudSimple_Symbole() {
		return (EAttribute)noeudSimpleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransition() {
		return transitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_StateInNoeudSimple() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_StateOutNoeudSimple() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_StateInComposite() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_StateOutComposite() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransition_Event() {
		return (EAttribute)transitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommentaire() {
		return commentaireEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommentaire_Name() {
		return (EAttribute)commentaireEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommentaire_Abs() {
		return (EAttribute)commentaireEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommentaire_Ord() {
		return (EAttribute)commentaireEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modelpsm_2Factory getModelpsm_2Factory() {
		return (Modelpsm_2Factory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		grammaireEClass = createEClass(GRAMMAIRE);
		createEReference(grammaireEClass, GRAMMAIRE__GRAPHES);
		createEAttribute(grammaireEClass, GRAMMAIRE__NAME);

		grapheEClass = createEClass(GRAPHE);
		createEReference(grapheEClass, GRAPHE__NOEUDS);
		createEReference(grapheEClass, GRAPHE__ASSOCIATIONS);
		createEReference(grapheEClass, GRAPHE__NOEUDS_COMPOSITE);
		createEAttribute(grapheEClass, GRAPHE__NAME);
		createEReference(grapheEClass, GRAPHE__COMMENTS);

		noeudCompositeEClass = createEClass(NOEUD_COMPOSITE);
		createEReference(noeudCompositeEClass, NOEUD_COMPOSITE__CMP_GRAPHES);
		createEAttribute(noeudCompositeEClass, NOEUD_COMPOSITE__NAME);
		createEAttribute(noeudCompositeEClass, NOEUD_COMPOSITE__ABS);
		createEAttribute(noeudCompositeEClass, NOEUD_COMPOSITE__ORD);
		createEAttribute(noeudCompositeEClass, NOEUD_COMPOSITE__NB_ETAT_LIE);
		createEAttribute(noeudCompositeEClass, NOEUD_COMPOSITE__SYMBOLE);

		noeudSimpleEClass = createEClass(NOEUD_SIMPLE);
		createEAttribute(noeudSimpleEClass, NOEUD_SIMPLE__NAME);
		createEAttribute(noeudSimpleEClass, NOEUD_SIMPLE__ABS);
		createEAttribute(noeudSimpleEClass, NOEUD_SIMPLE__ORD);
		createEAttribute(noeudSimpleEClass, NOEUD_SIMPLE__NB_ETAT_LIE);
		createEAttribute(noeudSimpleEClass, NOEUD_SIMPLE__SYMBOLE);

		transitionEClass = createEClass(TRANSITION);
		createEReference(transitionEClass, TRANSITION__STATE_IN_NOEUD_SIMPLE);
		createEReference(transitionEClass, TRANSITION__STATE_OUT_NOEUD_SIMPLE);
		createEReference(transitionEClass, TRANSITION__STATE_IN_COMPOSITE);
		createEReference(transitionEClass, TRANSITION__STATE_OUT_COMPOSITE);
		createEAttribute(transitionEClass, TRANSITION__EVENT);

		commentaireEClass = createEClass(COMMENTAIRE);
		createEAttribute(commentaireEClass, COMMENTAIRE__NAME);
		createEAttribute(commentaireEClass, COMMENTAIRE__ABS);
		createEAttribute(commentaireEClass, COMMENTAIRE__ORD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(grammaireEClass, Grammaire.class, "Grammaire", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGrammaire_Graphes(), this.getGraphe(), null, "graphes", null, 1, -1, Grammaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrammaire_Name(), ecorePackage.getEString(), "name", null, 1, 1, Grammaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(grapheEClass, Graphe.class, "Graphe", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGraphe_Noeuds(), this.getNoeudSimple(), null, "noeuds", null, 1, -1, Graphe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphe_Associations(), this.getTransition(), null, "associations", null, 1, -1, Graphe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphe_NoeudsComposite(), this.getNoeudComposite(), null, "noeudsComposite", null, 1, -1, Graphe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGraphe_Name(), ecorePackage.getEString(), "name", null, 1, 1, Graphe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGraphe_Comments(), this.getCommentaire(), null, "comments", null, 0, -1, Graphe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(noeudCompositeEClass, NoeudComposite.class, "NoeudComposite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNoeudComposite_CmpGraphes(), this.getGraphe(), null, "cmpGraphes", null, 1, -1, NoeudComposite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudComposite_Name(), ecorePackage.getEString(), "name", null, 1, 1, NoeudComposite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudComposite_Abs(), ecorePackage.getEBigInteger(), "abs", null, 0, 1, NoeudComposite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudComposite_Ord(), ecorePackage.getEBigInteger(), "ord", null, 0, 1, NoeudComposite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudComposite_NbEtatLie(), ecorePackage.getEInt(), "nbEtatLie", null, 0, 1, NoeudComposite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudComposite_Symbole(), ecorePackage.getEInt(), "symbole", null, 0, 1, NoeudComposite.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(noeudSimpleEClass, NoeudSimple.class, "NoeudSimple", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNoeudSimple_Name(), ecorePackage.getEString(), "name", null, 1, 1, NoeudSimple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudSimple_Abs(), ecorePackage.getEBigInteger(), "abs", null, 0, 1, NoeudSimple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudSimple_Ord(), ecorePackage.getEBigInteger(), "ord", null, 0, 1, NoeudSimple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudSimple_NbEtatLie(), ecorePackage.getEInt(), "nbEtatLie", null, 0, 1, NoeudSimple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNoeudSimple_Symbole(), ecorePackage.getEInt(), "symbole", null, 0, 1, NoeudSimple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transitionEClass, Transition.class, "Transition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransition_StateInNoeudSimple(), this.getNoeudSimple(), null, "stateInNoeudSimple", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_StateOutNoeudSimple(), this.getNoeudSimple(), null, "stateOutNoeudSimple", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_StateInComposite(), this.getNoeudComposite(), null, "stateInComposite", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_StateOutComposite(), this.getNoeudComposite(), null, "stateOutComposite", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransition_Event(), ecorePackage.getEString(), "event", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commentaireEClass, Commentaire.class, "Commentaire", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCommentaire_Name(), ecorePackage.getEString(), "name", null, 1, 1, Commentaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommentaire_Abs(), ecorePackage.getEBigInteger(), "abs", null, 0, 1, Commentaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommentaire_Ord(), ecorePackage.getEBigInteger(), "ord", null, 0, 1, Commentaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Modelpsm_2PackageImpl
