/**
 */
package modelpsm_2.impl;

import java.math.BigInteger;
import modelpsm_2.Modelpsm_2Package;
import modelpsm_2.NoeudSimple;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Noeud Simple</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link modelpsm_2.impl.NoeudSimpleImpl#getName <em>Name</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudSimpleImpl#getAbs <em>Abs</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudSimpleImpl#getOrd <em>Ord</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudSimpleImpl#getNbEtatLie <em>Nb Etat Lie</em>}</li>
 *   <li>{@link modelpsm_2.impl.NoeudSimpleImpl#getSymbole <em>Symbole</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NoeudSimpleImpl extends MinimalEObjectImpl.Container implements NoeudSimple {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getAbs() <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbs()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ABS_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getAbs() <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbs()
	 * @generated
	 * @ordered
	 */
	protected BigInteger abs = ABS_EDEFAULT;
	/**
	 * The default value of the '{@link #getOrd() <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrd()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ORD_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getOrd() <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrd()
	 * @generated
	 * @ordered
	 */
	protected BigInteger ord = ORD_EDEFAULT;
	/**
	 * The default value of the '{@link #getNbEtatLie() <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbEtatLie()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_ETAT_LIE_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getNbEtatLie() <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbEtatLie()
	 * @generated
	 * @ordered
	 */
	protected int nbEtatLie = NB_ETAT_LIE_EDEFAULT;
	/**
	 * The default value of the '{@link #getSymbole() <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbole()
	 * @generated
	 * @ordered
	 */
	protected static final int SYMBOLE_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getSymbole() <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSymbole()
	 * @generated
	 * @ordered
	 */
	protected int symbole = SYMBOLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NoeudSimpleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return Modelpsm_2Package.Literals.NOEUD_SIMPLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_SIMPLE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getAbs() {
		return abs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbs(BigInteger newAbs) {
		BigInteger oldAbs = abs;
		abs = newAbs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_SIMPLE__ABS, oldAbs, abs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getOrd() {
		return ord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrd(BigInteger newOrd) {
		BigInteger oldOrd = ord;
		ord = newOrd;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_SIMPLE__ORD, oldOrd, ord));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbEtatLie() {
		return nbEtatLie;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNbEtatLie(int newNbEtatLie) {
		int oldNbEtatLie = nbEtatLie;
		nbEtatLie = newNbEtatLie;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_SIMPLE__NB_ETAT_LIE, oldNbEtatLie, nbEtatLie));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSymbole() {
		return symbole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSymbole(int newSymbole) {
		int oldSymbole = symbole;
		symbole = newSymbole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Modelpsm_2Package.NOEUD_SIMPLE__SYMBOLE, oldSymbole, symbole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_SIMPLE__NAME:
				return getName();
			case Modelpsm_2Package.NOEUD_SIMPLE__ABS:
				return getAbs();
			case Modelpsm_2Package.NOEUD_SIMPLE__ORD:
				return getOrd();
			case Modelpsm_2Package.NOEUD_SIMPLE__NB_ETAT_LIE:
				return new Integer(getNbEtatLie());
			case Modelpsm_2Package.NOEUD_SIMPLE__SYMBOLE:
				return new Integer(getSymbole());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_SIMPLE__NAME:
				setName((String)newValue);
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__ABS:
				setAbs((BigInteger)newValue);
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__ORD:
				setOrd((BigInteger)newValue);
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__NB_ETAT_LIE:
				setNbEtatLie(((Integer)newValue).intValue());
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__SYMBOLE:
				setSymbole(((Integer)newValue).intValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_SIMPLE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__ABS:
				setAbs(ABS_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__ORD:
				setOrd(ORD_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__NB_ETAT_LIE:
				setNbEtatLie(NB_ETAT_LIE_EDEFAULT);
				return;
			case Modelpsm_2Package.NOEUD_SIMPLE__SYMBOLE:
				setSymbole(SYMBOLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Modelpsm_2Package.NOEUD_SIMPLE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Modelpsm_2Package.NOEUD_SIMPLE__ABS:
				return ABS_EDEFAULT == null ? abs != null : !ABS_EDEFAULT.equals(abs);
			case Modelpsm_2Package.NOEUD_SIMPLE__ORD:
				return ORD_EDEFAULT == null ? ord != null : !ORD_EDEFAULT.equals(ord);
			case Modelpsm_2Package.NOEUD_SIMPLE__NB_ETAT_LIE:
				return nbEtatLie != NB_ETAT_LIE_EDEFAULT;
			case Modelpsm_2Package.NOEUD_SIMPLE__SYMBOLE:
				return symbole != SYMBOLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", abs: ");
		result.append(abs);
		result.append(", ord: ");
		result.append(ord);
		result.append(", nbEtatLie: ");
		result.append(nbEtatLie);
		result.append(", symbole: ");
		result.append(symbole);
		result.append(')');
		return result.toString();
	}

} //NoeudSimpleImpl
