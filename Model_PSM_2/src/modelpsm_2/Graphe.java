/**
 */
package modelpsm_2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graphe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpsm_2.Graphe#getNoeuds <em>Noeuds</em>}</li>
 *   <li>{@link modelpsm_2.Graphe#getAssociations <em>Associations</em>}</li>
 *   <li>{@link modelpsm_2.Graphe#getNoeudsComposite <em>Noeuds Composite</em>}</li>
 *   <li>{@link modelpsm_2.Graphe#getName <em>Name</em>}</li>
 *   <li>{@link modelpsm_2.Graphe#getComments <em>Comments</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpsm_2.Modelpsm_2Package#getGraphe()
 * @model
 * @generated
 */
public interface Graphe extends EObject {
	/**
	 * Returns the value of the '<em><b>Noeuds</b></em>' containment reference list.
	 * The list contents are of type {@link modelpsm_2.NoeudSimple}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noeuds</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noeuds</em>' containment reference list.
	 * @see modelpsm_2.Modelpsm_2Package#getGraphe_Noeuds()
	 * @model type="modelpsm_2.NoeudSimple" containment="true" required="true"
	 * @generated
	 */
	EList getNoeuds();

	/**
	 * Returns the value of the '<em><b>Associations</b></em>' containment reference list.
	 * The list contents are of type {@link modelpsm_2.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associations</em>' containment reference list.
	 * @see modelpsm_2.Modelpsm_2Package#getGraphe_Associations()
	 * @model type="modelpsm_2.Transition" containment="true" required="true"
	 * @generated
	 */
	EList getAssociations();

	/**
	 * Returns the value of the '<em><b>Noeuds Composite</b></em>' containment reference list.
	 * The list contents are of type {@link modelpsm_2.NoeudComposite}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Noeuds Composite</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Noeuds Composite</em>' containment reference list.
	 * @see modelpsm_2.Modelpsm_2Package#getGraphe_NoeudsComposite()
	 * @model type="modelpsm_2.NoeudComposite" containment="true" required="true"
	 * @generated
	 */
	EList getNoeudsComposite();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelpsm_2.Modelpsm_2Package#getGraphe_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelpsm_2.Graphe#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link modelpsm_2.Commentaire}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see modelpsm_2.Modelpsm_2Package#getGraphe_Comments()
	 * @model type="modelpsm_2.Commentaire" containment="true"
	 * @generated
	 */
	EList getComments();

} // Graphe
