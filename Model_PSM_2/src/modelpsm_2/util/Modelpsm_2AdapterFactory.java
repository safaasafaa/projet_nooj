/**
 */
package modelpsm_2.util;

import modelpsm_2.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see modelpsm_2.Modelpsm_2Package
 * @generated
 */
public class Modelpsm_2AdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static Modelpsm_2Package modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Modelpsm_2AdapterFactory() {
		if (modelPackage == null) {
			modelPackage = Modelpsm_2Package.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Modelpsm_2Switch modelSwitch =
		new Modelpsm_2Switch() {
			public Object caseGrammaire(Grammaire object) {
				return createGrammaireAdapter();
			}
			public Object caseGraphe(Graphe object) {
				return createGrapheAdapter();
			}
			public Object caseNoeudComposite(NoeudComposite object) {
				return createNoeudCompositeAdapter();
			}
			public Object caseNoeudSimple(NoeudSimple object) {
				return createNoeudSimpleAdapter();
			}
			public Object caseTransition(Transition object) {
				return createTransitionAdapter();
			}
			public Object caseCommentaire(Commentaire object) {
				return createCommentaireAdapter();
			}
			public Object defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter)modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link modelpsm_2.Grammaire <em>Grammaire</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see modelpsm_2.Grammaire
	 * @generated
	 */
	public Adapter createGrammaireAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link modelpsm_2.Graphe <em>Graphe</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see modelpsm_2.Graphe
	 * @generated
	 */
	public Adapter createGrapheAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link modelpsm_2.NoeudComposite <em>Noeud Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see modelpsm_2.NoeudComposite
	 * @generated
	 */
	public Adapter createNoeudCompositeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link modelpsm_2.NoeudSimple <em>Noeud Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see modelpsm_2.NoeudSimple
	 * @generated
	 */
	public Adapter createNoeudSimpleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link modelpsm_2.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see modelpsm_2.Transition
	 * @generated
	 */
	public Adapter createTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link modelpsm_2.Commentaire <em>Commentaire</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see modelpsm_2.Commentaire
	 * @generated
	 */
	public Adapter createCommentaireAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //Modelpsm_2AdapterFactory
