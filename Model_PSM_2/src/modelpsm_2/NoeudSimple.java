/**
 */
package modelpsm_2;

import java.math.BigInteger;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Noeud Simple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpsm_2.NoeudSimple#getName <em>Name</em>}</li>
 *   <li>{@link modelpsm_2.NoeudSimple#getAbs <em>Abs</em>}</li>
 *   <li>{@link modelpsm_2.NoeudSimple#getOrd <em>Ord</em>}</li>
 *   <li>{@link modelpsm_2.NoeudSimple#getNbEtatLie <em>Nb Etat Lie</em>}</li>
 *   <li>{@link modelpsm_2.NoeudSimple#getSymbole <em>Symbole</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpsm_2.Modelpsm_2Package#getNoeudSimple()
 * @model
 * @generated
 */
public interface NoeudSimple extends EObject {

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelpsm_2.Modelpsm_2Package#getNoeudSimple_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelpsm_2.NoeudSimple#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abs</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abs</em>' attribute.
	 * @see #setAbs(BigInteger)
	 * @see modelpsm_2.Modelpsm_2Package#getNoeudSimple_Abs()
	 * @model
	 * @generated
	 */
	BigInteger getAbs();

	/**
	 * Sets the value of the '{@link modelpsm_2.NoeudSimple#getAbs <em>Abs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abs</em>' attribute.
	 * @see #getAbs()
	 * @generated
	 */
	void setAbs(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ord</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ord</em>' attribute.
	 * @see #setOrd(BigInteger)
	 * @see modelpsm_2.Modelpsm_2Package#getNoeudSimple_Ord()
	 * @model
	 * @generated
	 */
	BigInteger getOrd();

	/**
	 * Sets the value of the '{@link modelpsm_2.NoeudSimple#getOrd <em>Ord</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ord</em>' attribute.
	 * @see #getOrd()
	 * @generated
	 */
	void setOrd(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Nb Etat Lie</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nb Etat Lie</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Etat Lie</em>' attribute.
	 * @see #setNbEtatLie(int)
	 * @see modelpsm_2.Modelpsm_2Package#getNoeudSimple_NbEtatLie()
	 * @model
	 * @generated
	 */
	int getNbEtatLie();

	/**
	 * Sets the value of the '{@link modelpsm_2.NoeudSimple#getNbEtatLie <em>Nb Etat Lie</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Etat Lie</em>' attribute.
	 * @see #getNbEtatLie()
	 * @generated
	 */
	void setNbEtatLie(int value);

	/**
	 * Returns the value of the '<em><b>Symbole</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Symbole</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Symbole</em>' attribute.
	 * @see #setSymbole(int)
	 * @see modelpsm_2.Modelpsm_2Package#getNoeudSimple_Symbole()
	 * @model
	 * @generated
	 */
	int getSymbole();

	/**
	 * Sets the value of the '{@link modelpsm_2.NoeudSimple#getSymbole <em>Symbole</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Symbole</em>' attribute.
	 * @see #getSymbole()
	 * @generated
	 */
	void setSymbole(int value);
} // NoeudSimple
