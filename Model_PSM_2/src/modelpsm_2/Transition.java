/**
 */
package modelpsm_2;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpsm_2.Transition#getStateInNoeudSimple <em>State In Noeud Simple</em>}</li>
 *   <li>{@link modelpsm_2.Transition#getStateOutNoeudSimple <em>State Out Noeud Simple</em>}</li>
 *   <li>{@link modelpsm_2.Transition#getStateInComposite <em>State In Composite</em>}</li>
 *   <li>{@link modelpsm_2.Transition#getStateOutComposite <em>State Out Composite</em>}</li>
 *   <li>{@link modelpsm_2.Transition#getEvent <em>Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpsm_2.Modelpsm_2Package#getTransition()
 * @model
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>State In Noeud Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State In Noeud Simple</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State In Noeud Simple</em>' reference.
	 * @see #setStateInNoeudSimple(NoeudSimple)
	 * @see modelpsm_2.Modelpsm_2Package#getTransition_StateInNoeudSimple()
	 * @model required="true"
	 * @generated
	 */
	NoeudSimple getStateInNoeudSimple();

	/**
	 * Sets the value of the '{@link modelpsm_2.Transition#getStateInNoeudSimple <em>State In Noeud Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State In Noeud Simple</em>' reference.
	 * @see #getStateInNoeudSimple()
	 * @generated
	 */
	void setStateInNoeudSimple(NoeudSimple value);

	/**
	 * Returns the value of the '<em><b>State Out Noeud Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Out Noeud Simple</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Out Noeud Simple</em>' reference.
	 * @see #setStateOutNoeudSimple(NoeudSimple)
	 * @see modelpsm_2.Modelpsm_2Package#getTransition_StateOutNoeudSimple()
	 * @model required="true"
	 * @generated
	 */
	NoeudSimple getStateOutNoeudSimple();

	/**
	 * Sets the value of the '{@link modelpsm_2.Transition#getStateOutNoeudSimple <em>State Out Noeud Simple</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Out Noeud Simple</em>' reference.
	 * @see #getStateOutNoeudSimple()
	 * @generated
	 */
	void setStateOutNoeudSimple(NoeudSimple value);

	/**
	 * Returns the value of the '<em><b>State In Composite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State In Composite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State In Composite</em>' reference.
	 * @see #setStateInComposite(NoeudComposite)
	 * @see modelpsm_2.Modelpsm_2Package#getTransition_StateInComposite()
	 * @model required="true"
	 * @generated
	 */
	NoeudComposite getStateInComposite();

	/**
	 * Sets the value of the '{@link modelpsm_2.Transition#getStateInComposite <em>State In Composite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State In Composite</em>' reference.
	 * @see #getStateInComposite()
	 * @generated
	 */
	void setStateInComposite(NoeudComposite value);

	/**
	 * Returns the value of the '<em><b>State Out Composite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State Out Composite</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Out Composite</em>' reference.
	 * @see #setStateOutComposite(NoeudComposite)
	 * @see modelpsm_2.Modelpsm_2Package#getTransition_StateOutComposite()
	 * @model required="true"
	 * @generated
	 */
	NoeudComposite getStateOutComposite();

	/**
	 * Sets the value of the '{@link modelpsm_2.Transition#getStateOutComposite <em>State Out Composite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Out Composite</em>' reference.
	 * @see #getStateOutComposite()
	 * @generated
	 */
	void setStateOutComposite(NoeudComposite value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' attribute.
	 * @see #setEvent(String)
	 * @see modelpsm_2.Modelpsm_2Package#getTransition_Event()
	 * @model required="true"
	 * @generated
	 */
	String getEvent();

	/**
	 * Sets the value of the '{@link modelpsm_2.Transition#getEvent <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' attribute.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(String value);

} // Transition
