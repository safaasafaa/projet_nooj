/**
 */
package modelpsm_2;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see modelpsm_2.Modelpsm_2Package
 * @generated
 */
public interface Modelpsm_2Factory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Modelpsm_2Factory eINSTANCE = modelpsm_2.impl.Modelpsm_2FactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Grammaire</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Grammaire</em>'.
	 * @generated
	 */
	Grammaire createGrammaire();

	/**
	 * Returns a new object of class '<em>Graphe</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Graphe</em>'.
	 * @generated
	 */
	Graphe createGraphe();

	/**
	 * Returns a new object of class '<em>Noeud Composite</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Noeud Composite</em>'.
	 * @generated
	 */
	NoeudComposite createNoeudComposite();

	/**
	 * Returns a new object of class '<em>Noeud Simple</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Noeud Simple</em>'.
	 * @generated
	 */
	NoeudSimple createNoeudSimple();

	/**
	 * Returns a new object of class '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition</em>'.
	 * @generated
	 */
	Transition createTransition();

	/**
	 * Returns a new object of class '<em>Commentaire</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Commentaire</em>'.
	 * @generated
	 */
	Commentaire createCommentaire();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Modelpsm_2Package getModelpsm_2Package();

} //Modelpsm_2Factory
