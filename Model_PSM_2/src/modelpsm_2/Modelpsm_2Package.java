/**
 */
package modelpsm_2;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see modelpsm_2.Modelpsm_2Factory
 * @model kind="package"
 * @generated
 */
public interface Modelpsm_2Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "modelpsm_2";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://modelpsm_2/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "modelpsm_2";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Modelpsm_2Package eINSTANCE = modelpsm_2.impl.Modelpsm_2PackageImpl.init();

	/**
	 * The meta object id for the '{@link modelpsm_2.impl.GrammaireImpl <em>Grammaire</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpsm_2.impl.GrammaireImpl
	 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getGrammaire()
	 * @generated
	 */
	int GRAMMAIRE = 0;

	/**
	 * The feature id for the '<em><b>Graphes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAIRE__GRAPHES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAIRE__NAME = 1;

	/**
	 * The number of structural features of the '<em>Grammaire</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAMMAIRE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link modelpsm_2.impl.GrapheImpl <em>Graphe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpsm_2.impl.GrapheImpl
	 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getGraphe()
	 * @generated
	 */
	int GRAPHE = 1;

	/**
	 * The feature id for the '<em><b>Noeuds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE__NOEUDS = 0;

	/**
	 * The feature id for the '<em><b>Associations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE__ASSOCIATIONS = 1;

	/**
	 * The feature id for the '<em><b>Noeuds Composite</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE__NOEUDS_COMPOSITE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE__NAME = 3;

	/**
	 * The feature id for the '<em><b>Comments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE__COMMENTS = 4;

	/**
	 * The number of structural features of the '<em>Graphe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHE_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link modelpsm_2.impl.NoeudCompositeImpl <em>Noeud Composite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpsm_2.impl.NoeudCompositeImpl
	 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getNoeudComposite()
	 * @generated
	 */
	int NOEUD_COMPOSITE = 2;

	/**
	 * The feature id for the '<em><b>Cmp Graphes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_COMPOSITE__CMP_GRAPHES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_COMPOSITE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_COMPOSITE__ABS = 2;

	/**
	 * The feature id for the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_COMPOSITE__ORD = 3;

	/**
	 * The feature id for the '<em><b>Nb Etat Lie</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_COMPOSITE__NB_ETAT_LIE = 4;

	/**
	 * The feature id for the '<em><b>Symbole</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_COMPOSITE__SYMBOLE = 5;

	/**
	 * The number of structural features of the '<em>Noeud Composite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_COMPOSITE_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link modelpsm_2.impl.NoeudSimpleImpl <em>Noeud Simple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpsm_2.impl.NoeudSimpleImpl
	 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getNoeudSimple()
	 * @generated
	 */
	int NOEUD_SIMPLE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_SIMPLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_SIMPLE__ABS = 1;

	/**
	 * The feature id for the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_SIMPLE__ORD = 2;

	/**
	 * The feature id for the '<em><b>Nb Etat Lie</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_SIMPLE__NB_ETAT_LIE = 3;

	/**
	 * The feature id for the '<em><b>Symbole</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_SIMPLE__SYMBOLE = 4;

	/**
	 * The number of structural features of the '<em>Noeud Simple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOEUD_SIMPLE_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link modelpsm_2.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpsm_2.impl.TransitionImpl
	 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 4;

	/**
	 * The feature id for the '<em><b>State In Noeud Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STATE_IN_NOEUD_SIMPLE = 0;

	/**
	 * The feature id for the '<em><b>State Out Noeud Simple</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STATE_OUT_NOEUD_SIMPLE = 1;

	/**
	 * The feature id for the '<em><b>State In Composite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STATE_IN_COMPOSITE = 2;

	/**
	 * The feature id for the '<em><b>State Out Composite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__STATE_OUT_COMPOSITE = 3;

	/**
	 * The feature id for the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EVENT = 4;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link modelpsm_2.impl.CommentaireImpl <em>Commentaire</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see modelpsm_2.impl.CommentaireImpl
	 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getCommentaire()
	 * @generated
	 */
	int COMMENTAIRE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENTAIRE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Abs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENTAIRE__ABS = 1;

	/**
	 * The feature id for the '<em><b>Ord</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENTAIRE__ORD = 2;

	/**
	 * The number of structural features of the '<em>Commentaire</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENTAIRE_FEATURE_COUNT = 3;

	/**
	 * Returns the meta object for class '{@link modelpsm_2.Grammaire <em>Grammaire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grammaire</em>'.
	 * @see modelpsm_2.Grammaire
	 * @generated
	 */
	EClass getGrammaire();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpsm_2.Grammaire#getGraphes <em>Graphes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Graphes</em>'.
	 * @see modelpsm_2.Grammaire#getGraphes()
	 * @see #getGrammaire()
	 * @generated
	 */
	EReference getGrammaire_Graphes();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.Grammaire#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpsm_2.Grammaire#getName()
	 * @see #getGrammaire()
	 * @generated
	 */
	EAttribute getGrammaire_Name();

	/**
	 * Returns the meta object for class '{@link modelpsm_2.Graphe <em>Graphe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graphe</em>'.
	 * @see modelpsm_2.Graphe
	 * @generated
	 */
	EClass getGraphe();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpsm_2.Graphe#getNoeuds <em>Noeuds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Noeuds</em>'.
	 * @see modelpsm_2.Graphe#getNoeuds()
	 * @see #getGraphe()
	 * @generated
	 */
	EReference getGraphe_Noeuds();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpsm_2.Graphe#getAssociations <em>Associations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Associations</em>'.
	 * @see modelpsm_2.Graphe#getAssociations()
	 * @see #getGraphe()
	 * @generated
	 */
	EReference getGraphe_Associations();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpsm_2.Graphe#getNoeudsComposite <em>Noeuds Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Noeuds Composite</em>'.
	 * @see modelpsm_2.Graphe#getNoeudsComposite()
	 * @see #getGraphe()
	 * @generated
	 */
	EReference getGraphe_NoeudsComposite();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.Graphe#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpsm_2.Graphe#getName()
	 * @see #getGraphe()
	 * @generated
	 */
	EAttribute getGraphe_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpsm_2.Graphe#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comments</em>'.
	 * @see modelpsm_2.Graphe#getComments()
	 * @see #getGraphe()
	 * @generated
	 */
	EReference getGraphe_Comments();

	/**
	 * Returns the meta object for class '{@link modelpsm_2.NoeudComposite <em>Noeud Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Noeud Composite</em>'.
	 * @see modelpsm_2.NoeudComposite
	 * @generated
	 */
	EClass getNoeudComposite();

	/**
	 * Returns the meta object for the containment reference list '{@link modelpsm_2.NoeudComposite#getCmpGraphes <em>Cmp Graphes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cmp Graphes</em>'.
	 * @see modelpsm_2.NoeudComposite#getCmpGraphes()
	 * @see #getNoeudComposite()
	 * @generated
	 */
	EReference getNoeudComposite_CmpGraphes();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudComposite#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpsm_2.NoeudComposite#getName()
	 * @see #getNoeudComposite()
	 * @generated
	 */
	EAttribute getNoeudComposite_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudComposite#getAbs <em>Abs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abs</em>'.
	 * @see modelpsm_2.NoeudComposite#getAbs()
	 * @see #getNoeudComposite()
	 * @generated
	 */
	EAttribute getNoeudComposite_Abs();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudComposite#getOrd <em>Ord</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ord</em>'.
	 * @see modelpsm_2.NoeudComposite#getOrd()
	 * @see #getNoeudComposite()
	 * @generated
	 */
	EAttribute getNoeudComposite_Ord();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudComposite#getNbEtatLie <em>Nb Etat Lie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Etat Lie</em>'.
	 * @see modelpsm_2.NoeudComposite#getNbEtatLie()
	 * @see #getNoeudComposite()
	 * @generated
	 */
	EAttribute getNoeudComposite_NbEtatLie();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudComposite#getSymbole <em>Symbole</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbole</em>'.
	 * @see modelpsm_2.NoeudComposite#getSymbole()
	 * @see #getNoeudComposite()
	 * @generated
	 */
	EAttribute getNoeudComposite_Symbole();

	/**
	 * Returns the meta object for class '{@link modelpsm_2.NoeudSimple <em>Noeud Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Noeud Simple</em>'.
	 * @see modelpsm_2.NoeudSimple
	 * @generated
	 */
	EClass getNoeudSimple();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudSimple#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpsm_2.NoeudSimple#getName()
	 * @see #getNoeudSimple()
	 * @generated
	 */
	EAttribute getNoeudSimple_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudSimple#getAbs <em>Abs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abs</em>'.
	 * @see modelpsm_2.NoeudSimple#getAbs()
	 * @see #getNoeudSimple()
	 * @generated
	 */
	EAttribute getNoeudSimple_Abs();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudSimple#getOrd <em>Ord</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ord</em>'.
	 * @see modelpsm_2.NoeudSimple#getOrd()
	 * @see #getNoeudSimple()
	 * @generated
	 */
	EAttribute getNoeudSimple_Ord();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudSimple#getNbEtatLie <em>Nb Etat Lie</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Etat Lie</em>'.
	 * @see modelpsm_2.NoeudSimple#getNbEtatLie()
	 * @see #getNoeudSimple()
	 * @generated
	 */
	EAttribute getNoeudSimple_NbEtatLie();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.NoeudSimple#getSymbole <em>Symbole</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Symbole</em>'.
	 * @see modelpsm_2.NoeudSimple#getSymbole()
	 * @see #getNoeudSimple()
	 * @generated
	 */
	EAttribute getNoeudSimple_Symbole();

	/**
	 * Returns the meta object for class '{@link modelpsm_2.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see modelpsm_2.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference '{@link modelpsm_2.Transition#getStateInNoeudSimple <em>State In Noeud Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State In Noeud Simple</em>'.
	 * @see modelpsm_2.Transition#getStateInNoeudSimple()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_StateInNoeudSimple();

	/**
	 * Returns the meta object for the reference '{@link modelpsm_2.Transition#getStateOutNoeudSimple <em>State Out Noeud Simple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State Out Noeud Simple</em>'.
	 * @see modelpsm_2.Transition#getStateOutNoeudSimple()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_StateOutNoeudSimple();

	/**
	 * Returns the meta object for the reference '{@link modelpsm_2.Transition#getStateInComposite <em>State In Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State In Composite</em>'.
	 * @see modelpsm_2.Transition#getStateInComposite()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_StateInComposite();

	/**
	 * Returns the meta object for the reference '{@link modelpsm_2.Transition#getStateOutComposite <em>State Out Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State Out Composite</em>'.
	 * @see modelpsm_2.Transition#getStateOutComposite()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_StateOutComposite();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.Transition#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event</em>'.
	 * @see modelpsm_2.Transition#getEvent()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Event();

	/**
	 * Returns the meta object for class '{@link modelpsm_2.Commentaire <em>Commentaire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Commentaire</em>'.
	 * @see modelpsm_2.Commentaire
	 * @generated
	 */
	EClass getCommentaire();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.Commentaire#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see modelpsm_2.Commentaire#getName()
	 * @see #getCommentaire()
	 * @generated
	 */
	EAttribute getCommentaire_Name();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.Commentaire#getAbs <em>Abs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abs</em>'.
	 * @see modelpsm_2.Commentaire#getAbs()
	 * @see #getCommentaire()
	 * @generated
	 */
	EAttribute getCommentaire_Abs();

	/**
	 * Returns the meta object for the attribute '{@link modelpsm_2.Commentaire#getOrd <em>Ord</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ord</em>'.
	 * @see modelpsm_2.Commentaire#getOrd()
	 * @see #getCommentaire()
	 * @generated
	 */
	EAttribute getCommentaire_Ord();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Modelpsm_2Factory getModelpsm_2Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link modelpsm_2.impl.GrammaireImpl <em>Grammaire</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpsm_2.impl.GrammaireImpl
		 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getGrammaire()
		 * @generated
		 */
		EClass GRAMMAIRE = eINSTANCE.getGrammaire();

		/**
		 * The meta object literal for the '<em><b>Graphes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAMMAIRE__GRAPHES = eINSTANCE.getGrammaire_Graphes();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAMMAIRE__NAME = eINSTANCE.getGrammaire_Name();

		/**
		 * The meta object literal for the '{@link modelpsm_2.impl.GrapheImpl <em>Graphe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpsm_2.impl.GrapheImpl
		 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getGraphe()
		 * @generated
		 */
		EClass GRAPHE = eINSTANCE.getGraphe();

		/**
		 * The meta object literal for the '<em><b>Noeuds</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPHE__NOEUDS = eINSTANCE.getGraphe_Noeuds();

		/**
		 * The meta object literal for the '<em><b>Associations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPHE__ASSOCIATIONS = eINSTANCE.getGraphe_Associations();

		/**
		 * The meta object literal for the '<em><b>Noeuds Composite</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPHE__NOEUDS_COMPOSITE = eINSTANCE.getGraphe_NoeudsComposite();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRAPHE__NAME = eINSTANCE.getGraphe_Name();

		/**
		 * The meta object literal for the '<em><b>Comments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRAPHE__COMMENTS = eINSTANCE.getGraphe_Comments();

		/**
		 * The meta object literal for the '{@link modelpsm_2.impl.NoeudCompositeImpl <em>Noeud Composite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpsm_2.impl.NoeudCompositeImpl
		 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getNoeudComposite()
		 * @generated
		 */
		EClass NOEUD_COMPOSITE = eINSTANCE.getNoeudComposite();

		/**
		 * The meta object literal for the '<em><b>Cmp Graphes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOEUD_COMPOSITE__CMP_GRAPHES = eINSTANCE.getNoeudComposite_CmpGraphes();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_COMPOSITE__NAME = eINSTANCE.getNoeudComposite_Name();

		/**
		 * The meta object literal for the '<em><b>Abs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_COMPOSITE__ABS = eINSTANCE.getNoeudComposite_Abs();

		/**
		 * The meta object literal for the '<em><b>Ord</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_COMPOSITE__ORD = eINSTANCE.getNoeudComposite_Ord();

		/**
		 * The meta object literal for the '<em><b>Nb Etat Lie</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_COMPOSITE__NB_ETAT_LIE = eINSTANCE.getNoeudComposite_NbEtatLie();

		/**
		 * The meta object literal for the '<em><b>Symbole</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_COMPOSITE__SYMBOLE = eINSTANCE.getNoeudComposite_Symbole();

		/**
		 * The meta object literal for the '{@link modelpsm_2.impl.NoeudSimpleImpl <em>Noeud Simple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpsm_2.impl.NoeudSimpleImpl
		 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getNoeudSimple()
		 * @generated
		 */
		EClass NOEUD_SIMPLE = eINSTANCE.getNoeudSimple();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_SIMPLE__NAME = eINSTANCE.getNoeudSimple_Name();

		/**
		 * The meta object literal for the '<em><b>Abs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_SIMPLE__ABS = eINSTANCE.getNoeudSimple_Abs();

		/**
		 * The meta object literal for the '<em><b>Ord</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_SIMPLE__ORD = eINSTANCE.getNoeudSimple_Ord();

		/**
		 * The meta object literal for the '<em><b>Nb Etat Lie</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_SIMPLE__NB_ETAT_LIE = eINSTANCE.getNoeudSimple_NbEtatLie();

		/**
		 * The meta object literal for the '<em><b>Symbole</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOEUD_SIMPLE__SYMBOLE = eINSTANCE.getNoeudSimple_Symbole();

		/**
		 * The meta object literal for the '{@link modelpsm_2.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpsm_2.impl.TransitionImpl
		 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>State In Noeud Simple</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__STATE_IN_NOEUD_SIMPLE = eINSTANCE.getTransition_StateInNoeudSimple();

		/**
		 * The meta object literal for the '<em><b>State Out Noeud Simple</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__STATE_OUT_NOEUD_SIMPLE = eINSTANCE.getTransition_StateOutNoeudSimple();

		/**
		 * The meta object literal for the '<em><b>State In Composite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__STATE_IN_COMPOSITE = eINSTANCE.getTransition_StateInComposite();

		/**
		 * The meta object literal for the '<em><b>State Out Composite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__STATE_OUT_COMPOSITE = eINSTANCE.getTransition_StateOutComposite();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__EVENT = eINSTANCE.getTransition_Event();

		/**
		 * The meta object literal for the '{@link modelpsm_2.impl.CommentaireImpl <em>Commentaire</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see modelpsm_2.impl.CommentaireImpl
		 * @see modelpsm_2.impl.Modelpsm_2PackageImpl#getCommentaire()
		 * @generated
		 */
		EClass COMMENTAIRE = eINSTANCE.getCommentaire();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENTAIRE__NAME = eINSTANCE.getCommentaire_Name();

		/**
		 * The meta object literal for the '<em><b>Abs</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENTAIRE__ABS = eINSTANCE.getCommentaire_Abs();

		/**
		 * The meta object literal for the '<em><b>Ord</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMENTAIRE__ORD = eINSTANCE.getCommentaire_Ord();

	}

} //Modelpsm_2Package
