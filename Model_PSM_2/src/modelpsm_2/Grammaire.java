/**
 */
package modelpsm_2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grammaire</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link modelpsm_2.Grammaire#getGraphes <em>Graphes</em>}</li>
 *   <li>{@link modelpsm_2.Grammaire#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see modelpsm_2.Modelpsm_2Package#getGrammaire()
 * @model
 * @generated
 */
public interface Grammaire extends EObject {
	/**
	 * Returns the value of the '<em><b>Graphes</b></em>' containment reference list.
	 * The list contents are of type {@link modelpsm_2.Graphe}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graphes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graphes</em>' containment reference list.
	 * @see modelpsm_2.Modelpsm_2Package#getGrammaire_Graphes()
	 * @model type="modelpsm_2.Graphe" containment="true" required="true"
	 * @generated
	 */
	EList getGraphes();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see modelpsm_2.Modelpsm_2Package#getGrammaire_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link modelpsm_2.Grammaire#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Grammaire
